# Reckon API Sample App  
___

***NOTE:** This Reckon API Sample App is generated as an illustration of creating requests to the different endpoints of the Reckon API ONLY.*

*It is NOT intended to show best practices or ideal code.*

*Use at your own discretion.*

___

The Reckon API Sample App was written in C# as a Windows Forms Application.  It uses a MySQL database to store and retrieve data.

It provide examples of the Authorization process, and API requests to both the Reckon Accounts Hosted API and the Reckon One API.  We don't include examples of parsing or processing Reckon Accounts Hosted API or Reckon One API responses, we simply output the responses body to the screen.

To utilize the Reckon API Sample App you will need a copy of Visual Studio installed, as well as MySQL Server.  We chose to use WampServer - [http://www.wampserver.com/en/](http://www.wampserver.com/en/) - as its a free and easy to use web server that includes MySQL Server.  We recommend you use WampServer also to minimize difficulties that may arise.

Open Visual Studio and clone the Reckon API Sample App Repository.  The default location it will be cloned to is **C:\Users\\<<user>>\source\repos\reckon-api-sample-app\\**.  It will include a file named **Reckon api sample app.sql** which is the database the sample app uses.  

Next start WampServer via the start menu.  You should see the WampServer icon in the notifications area ![ ](ReadMeImages/WampLogo.jpg).

If the icon is not green left click on the icon and select **Start All Services**.

![ ](ReadMeImages/WampMenu.jpg)

Open the MySQL console by left clicking the WampServer icon, select MySQL, then MySQL Console.

![ ](ReadMeImages/MySQLMenu.jpg)

The MySQL console will open and prompt you for a password.  Enter it and/or press enter.

![ ](ReadMeImages/MySQLConsole.jpg)

You can now import the database with the command **\\\. C:\Users\\<<user>>\source\repos\reckon-api-sample-app\Reckon api sample app.sql**

![ ](ReadMeImages/DatabasePath.jpg)

Some of the tables in the database will be prefilled with data.  Do not delete this prefilled data.  It will be updated as you use the Reckon API Sample App.

Open the solution in Visual Studio.  In Solution Explorer find **App.config**.  This file is used to store your client id, client secret, redirect url, subscription keys, and MySQL connection details.  

![ ](ReadMeImages/Appconfig.jpg)

Your client id and client secret would have been provided to you when your Reckon Developer Partner application was approved, and the redirect url will be the one you provided to us.  The subscription keys for Reckon One and Reckon Accounts Hosted can be found in the Reckon Developer Portal in the profile area.  If you are missing a subscription key email apisupport@reckon.com and let us know which one is missing.

The default MySQL connection details will be:

* Server: localhost
* Database: reckon api sample app
* User: root
* Password: (*leave blank*)
* Port: 3306
* SSL Mode: none

Once you have filled in App.config you should be able to run the solution.

We have created classes for each section of the API.  

The Authorization class will contain a general token request, which is then used to make token and refresh token requests.

The Reckon Accounts Hosted class will contain all Reckon Accounts Hosted API requests such as the Reckon Accounts Heartbeat request, Get Supported Version request, Retrieve GUID request, and Reckon Accounts Hosted V2 api requests.

The Reckon One class will contain all Reckon One API requests such as Reckon One Heartbeat request, Get Cashbooks request, and GET, PUT, POST and DELETE requests to Reckon endpoints.

If you have any questions about the sample app email the API team on apisupport@reckon.com.

___	