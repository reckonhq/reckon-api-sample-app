/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.7.14 : Database - reckon api sample app
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`reckon api sample app` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `reckon api sample app`;

/*Table structure for table `cashbooks` */

DROP TABLE IF EXISTS `cashbooks`;

CREATE TABLE `cashbooks` (
  `BookId` varchar(250) NOT NULL,
  `BookName` varchar(250) DEFAULT NULL,
  `Bookstatus` varchar(250) DEFAULT NULL,
  `Country` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`BookId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `cashbooks` */

/*Table structure for table `r1auth` */

DROP TABLE IF EXISTS `r1auth`;

CREATE TABLE `r1auth` (
  `accessToken` varchar(250) DEFAULT NULL,
  `refreshToken` varchar(250) DEFAULT NULL,
  `tokenExpiry` varchar(250) DEFAULT NULL,
  `tokenType` varchar(250) DEFAULT NULL,
  `authTime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 MIN_ROWS=1 MAX_ROWS=1;

/*Data for the table `r1auth` */

insert  into `r1auth`(`accessToken`,`refreshToken`,`tokenExpiry`,`tokenType`,`authTime`) values ('111111','111111','10800','Bearer','0000-00-00 00:00:00');

/*Table structure for table `rahauth` */

DROP TABLE IF EXISTS `rahauth`;

CREATE TABLE `rahauth` (
  `accessToken` varchar(250) DEFAULT NULL,
  `refreshToken` varchar(250) DEFAULT NULL,
  `tokenExpiry` varchar(250) DEFAULT NULL,
  `tokenType` varchar(250) DEFAULT NULL,
  `authTime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 MIN_ROWS=1 MAX_ROWS=1;

/*Data for the table `rahauth` */

insert  into `rahauth`(`accessToken`,`refreshToken`,`tokenExpiry`,`tokenType`,`authTime`) values ('111111','111111','10800','Bearer','0000-00-00 00:00:00');

/*Table structure for table `rahfiledetails` */

DROP TABLE IF EXISTS `rahfiledetails`;

CREATE TABLE `rahfiledetails` (
  `filepath` varchar(250) DEFAULT NULL,
  `fileuser` varchar(250) DEFAULT NULL,
  `filepassword` varchar(250) DEFAULT NULL,
  `countryversion` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `rahfiledetails` */

insert  into `rahfiledetails`(`filepath`,`fileuser`,`filepassword`,`countryversion`) values ('Q:\\CompanyFile.QBW','Admin','Password','Version');

/*Table structure for table `rahsupportedversions` */

DROP TABLE IF EXISTS `rahsupportedversions`;

CREATE TABLE `rahsupportedversions` (
  `countryVersion` varchar(250) NOT NULL,
  PRIMARY KEY (`countryVersion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `rahsupportedversions` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
