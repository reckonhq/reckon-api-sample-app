﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System;
using System.Configuration;

namespace Reckon_API_Sample_App
{

    //Class holding Reckon Accounts Hosted functions and fields
    class ReckonAccountsHosted
    {
        public ReckonAccountsHosted() { }
        private Form1 _instance;
        public ReckonAccountsHosted(Form1 instance)
        {
            _instance = instance;
        }

        public static string FilePath { get; set; }
        public static string FileUserName { get; set; }
        public static string FilePassword { get; set; }
        public static string CountryVersion { get; set; }
        public static string RahSubscriptionKey { get; } = ConfigurationManager.AppSettings["rahSubscriptionKey"].ToString();
        private static string RahApiSegment { get; } = "/RAH/v2";
        public static string RahSupportedVersions { get; } = "/SupportedVersions";
        public static string RahHeartbeat { get; } = "/Heartbeat";

        public static string Server { get; } = ConfigurationManager.AppSettings["server"].ToString();
        public static string Database { get; } = ConfigurationManager.AppSettings["database"].ToString();
        public static string User { get; } = ConfigurationManager.AppSettings["user"].ToString();
        public static string Password { get; } = ConfigurationManager.AppSettings["password"].ToString();
        public static string Port { get; } = ConfigurationManager.AppSettings["port"].ToString();
        public static string SSLMode { get; } = ConfigurationManager.AppSettings["sslMode"].ToString();

        public string RequestId { get; set; }
        public string Data { get; set; }
        public string Success { get; set; }
        public string RetryLater { get; set; }
        public string Message { get; set; }

        //Check RAH Heartbeat
        public void RAHHearbeat()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var response = httpClient.GetAsync($"{ Globals.ApiBaseUrl }{ RahApiSegment }{ RahHeartbeat }").Result;
                Globals.RahStatus = response.StatusCode.ToString();
            }
        }
       
        //Reckon Accounts Hosted API POST Request
        public void RahApiRequest()
        {
            var version = "";
            _instance.VersionCombo.Invoke((Action)delegate { version = _instance.VersionCombo.Text; });
            JsonSerializer jsonSerializer = new JsonSerializer();
            StringWriter stringWriter = new StringWriter();
            jsonSerializer.Serialize(stringWriter, new
            {
                FileName = _instance.FilePathText.Text,
                Operation = _instance.RahBodyText.Text,
                CountryVersion = version,
                UserName = _instance.FileUserText.Text,
                Password = _instance.FilePwdText.Text
            });
            string jsonData = stringWriter.ToString();
            StringContent stringContent = new StringContent(jsonData);
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, RahSubscriptionKey);
                var response = httpClient.PostAsync($"{ Globals.ApiBaseUrl }{ RahApiSegment }", stringContent).Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                string responseStatusCode = response.StatusCode.ToString();
                if (responseStatusCode != "OK")
                {
                    _instance.RahPostButton.Invoke((Action)delegate { _instance.RahPostButton.Enabled = true; });
                    _instance.RahPostButton.Invoke((Action)delegate { _instance.RahPostButton.Text = "POST"; });
                    _instance.RahPostButton.Invoke((Action)delegate { _instance.RahPostButton.BackColor = Color.FromArgb(0, 189, 157); });
                    _instance.RahRespText.Invoke((Action)delegate { _instance.RahRespText.Text = responseText; });
                    MessageBox.Show("There was an error posting data to " + Globals.ApiBaseUrl + RahApiSegment + 
                        ". The response status code is " + responseStatusCode, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    var result = JsonConvert.DeserializeObject<ReckonAccountsHosted>(responseText);
                    ReckonAccountsHosted rahResponse = new ReckonAccountsHosted
                    {
                        RequestId = result.RequestId,
                        Data = result.Data,
                        Success = result.Success,
                        RetryLater = result.RetryLater,
                        Message = result.Message
                    };
                    if (rahResponse.RequestId != null)
                    {
                        _instance.RahRqIdText.Invoke((Action)delegate { _instance.RahRqIdText.Text = rahResponse.RequestId; });
                        _instance.RahRespText.Invoke((Action)delegate { _instance.RahRespText.Text = responseText; });
                        RequestId = rahResponse.RequestId;
                        MessageBox.Show("The response time limit has been reached but the request is still processing. " +
                            "\r\nWait a few minutes and use the request id to retreive the response.", "Request Time Limit Reached!",
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else _instance.RahRespText.Invoke((Action)delegate { _instance.RahRespText.Text = rahResponse.Data; });
                }
            }
        }

        //Reckon Accounts Hosted API GET GUID Request
        public void RahGetGuidRequest()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, RahSubscriptionKey);
                var response = httpClient.GetAsync($"{ Globals.ApiBaseUrl }{ RahApiSegment }/{ _instance.RahGuid}").Result;
                var responseText = response.Content.ReadAsStringAsync().Result;
                string responseStatusCode = response.StatusCode.ToString();
                if (responseStatusCode != "OK")
                {
                    _instance.RahGetIdButton.Invoke((Action)delegate { _instance.RahGetIdButton.Enabled = true; });
                    _instance.RahGetIdButton.Invoke((Action)delegate { _instance.RahGetIdButton.Text = "POST"; });
                    _instance.RahGetIdButton.Invoke((Action)delegate { _instance.RahGetIdButton.BackColor = Color.FromArgb(0, 189, 157); });
                    _instance.RahRespText.Invoke((Action)delegate { _instance.RahRespText.Text = responseText; });
                    MessageBox.Show("There was an error posting data to " + Globals.ApiBaseUrl + RahApiSegment + 
                        ". The response status code is " + responseStatusCode, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    var result = JsonConvert.DeserializeObject<ReckonAccountsHosted>(responseText);
                    ReckonAccountsHosted rahResponse = new ReckonAccountsHosted
                    {
                        RequestId = result.RequestId,
                        Data = result.Data,
                        Success = result.Success,
                        RetryLater = result.RetryLater,
                        Message = result.Message
                    };
                    if (rahResponse.RequestId != null)
                    {
                        _instance.RahRqIdText.Invoke((Action)delegate { _instance.RahRqIdText.Text = rahResponse.RequestId; });
                        _instance.RahRespText.Invoke((Action)delegate { _instance.RahRespText.Text = responseText; });
                        RequestId = rahResponse.RequestId;
                        MessageBox.Show("The response time limit has been reached but the request is still processing. " +
                            "\r\nWait a few minutes and use the request id to retreive the response.", "Request Time Limit Reached!", 
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else _instance.RahRespText.Invoke((Action)delegate { _instance.RahRespText.Text = rahResponse.Data; }); 
                }
            }
        }

        //Reckon Accounts Hosted QBXML to be used based on combobox selection
        public void RAHFunctions()
        {
            string functions = _instance.RahFunctionsComboBox;
            switch (functions)
            {
                case "POST Host(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<HostQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Company(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CompanyQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST CompanyActivity(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CompanyActivityQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Preferences(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<PreferencesQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Account(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<AccountQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Customer(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CustomerQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Employee(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<EmployeeQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST OtherName(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<OtherNameQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Vendor(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<VendorQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Entity(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<EntityQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST StandardTerms(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<StandardTermsQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST DateDrivenTerms(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<DateDrivenTermsQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Terms(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<TermsQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Class(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ClassQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST SalesRep(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<SalesRepQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemService(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemServiceQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemNonInventory(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemNonInventoryQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemOtherCharge(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemOtherChargeQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemInventory(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemInventoryQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemInventoryAssembly(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemInventoryAssemblyQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemSubtotal(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemSubtotalQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemDiscount(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemDiscountQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemPayment(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemPaymentQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemSalesTax(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemSalesTaxQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemGroup(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemGroupQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Item(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Template(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<TemplateQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Invoice(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<InvoiceQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Estimate(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<EstimateQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST SalesOrder(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<SalesOrderQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST SalesReceipt(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<SalesReceiptQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST CreditMemo(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CreditMemoQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ReceivePayment(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ReceivePaymentQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ReceivePaymentToDeposit(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ReceivePaymentToDepositQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST PurchaseOrder(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<PurchaseOrderQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Bill(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<BillQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemReceipt(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemReceiptQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST VendorCredit(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<VendorCreditQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST BillPaymentCheck(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<BillPaymentCheckQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST BillPaymentCreditCard(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<BillPaymentCreditCardQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST InventoryAdjustment(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<InventoryAdjustmentQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST TimeTracking(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<TimeTrackingQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Check(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CheckQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST CreditCardCharge(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CreditCardChargeQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST CreditCardCredit(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CreditCardCreditQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST JournalEntry(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<JournalEntryQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Deposit(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<DepositQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST PriceLevel(Query)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<PriceLevelQueryRq />\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Account(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<AccountAddRq requestID=\"1\">\r\n\t\t<AccountAdd>\r\n\t\t\t<Name>Special Events Expense Account</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t\t<AccountType>Expense</AccountType>\r\n\t\t\t<Desc>Special events expense account</Desc>\r\n\t\t</AccountAdd>\r\n\t</AccountAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Customer(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CustomerAddRq requestID=\"1\">\r\n\t\t<CustomerAdd>\r\n\t\t\t<Name>Joes Hardware Sydney</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t\t<CompanyName>Joes Hardware Sydney</CompanyName>\r\n\t\t\t<Salutation>Mr</Salutation>\r\r\n\t\t\t<FirstName>Joe</FirstName>\r\n\t\t\t<LastName>Smith</LastName>\r\n\t\t\t<BillAddress>\r\n\t\t\t\t<Addr1>100 Pacific Highway North Sydney</Addr1>\r\n\t\t\t\t<City>Sydney</City>\r\n\t\t\t\t<State>NSW</State>\r\n\t\t\t\t<PostalCode>2060</PostalCode>\r\n\t\t\t\t<Country>Australia</Country>\r\n\t\t\t</BillAddress>\r\n\t\t\t<ShipAddress>\r\n\t\t\t\t<Addr1>100 Pacific Highway North Sydney</Addr1>\r\n\t\t\t\t<City>Sydney</City>\r\n\t\t\t\t<State>NSW</State>\r\n\t\t\t\t<PostalCode>2060</PostalCode>\r\n\t\t\t\t<Country>Australia</Country>\r\n\t\t\t</ShipAddress>\r\n\t\t\t\t<Phone>0295775000</Phone>\r\n\t\t\t<Email>psgsupport@reckon.com</Email>\r\n\t\t\t<Contact>Joe Smith</Contact>\r\n\t\t</CustomerAdd>\r\n\t</CustomerAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Vendor(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<VendorAddRq requestID=\"1\">\r\n\t\t<VendorAdd>\r\n\t\t\t<Name>Harries Supplies</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t\t<CompanyName>Harries Supplies</CompanyName>\r\n\t\t\t<Salutation>Mr</Salutation>\r\r\n\t\t\t<FirstName>Harry</FirstName>\r\n\t\t\t<LastName>Jones</LastName>\r\n\t\t\t<VendorAddress>\r\n\t\t\t\t<Addr1>52 Berry Street North Sydney</Addr1>\r\n\t\t\t\t<City>Sydney</City>\r\n\t\t\t\t<State>NSW</State>\r\n\t\t\t\t<PostalCode>2060</PostalCode>\r\n\t\t\t\t<Country>Australia</Country>\r\n\t\t\t</VendorAddress>\r\n\t\t\t<Phone>0295775000</Phone>\r\n\t\t\t<Email>psgsupport@reckon.com</Email>\r\n\t\t\t<Contact>Harry Jones</Contact>\r\n\t\t</VendorAdd>\r\n\t</VendorAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Class(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ClassAddRq requestID=\"1\">\r\n\t\t<ClassAdd>\r\n\t\t\t<Name>Newcastle</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t</ClassAdd>\r\n\t</ClassAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemService(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemServiceAddRq requestID=\"1\">\r\n\t\t<ItemServiceAdd>\r\n\t\t\t<Name>Special Accounting</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t</SalesTaxCodeRef>\r\n\t\t\t<SalesOrPurchase>\r\n\t\t\t\t<Desc>Special Accounting Service</Desc>\r\n\t\t\t\t<Price>120.00</Price>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<FullName>Sales</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t</SalesOrPurchase>\r\n\t\t</ItemServiceAdd>\r\n\t</ItemServiceAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemNonInventory(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemNonInventoryAddRq requestID=\"1\">\r\n\t\t<ItemNonInventoryAdd>\r\n\t\t\t<Name>Office Chairs</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t</SalesTaxCodeRef>\r\n\t\t\t<SalesOrPurchase>\r\n\t\t\t\t<Desc>Office Chairs</Desc>\r\n\t\t\t\t<Price>40.00</Price>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<FullName>Sales</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t</SalesOrPurchase>\r\n\t\t</ItemNonInventoryAdd>\r\n\t</ItemNonInventoryAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemInventory(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemInventoryAddRq requestID=\"1\">\r\n\t\t<ItemInventoryAdd>\r\n\t\t\t<Name>Hammer</Name>\r\n\t\t\t<IsActive>true</IsActive>\r\n\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t</SalesTaxCodeRef>\r\n\t\t\t<SalesDesc>Hammer</SalesDesc>\r\n\t\t\t<SalesPrice>10.00</SalesPrice>\r\n\t\t\t<IncomeAccountRef>\r\n\t\t\t\t<FullName>Sales</FullName>\r\n\t\t\t</IncomeAccountRef>\r\n\t\t\t<PurchaseDesc>Hammer</PurchaseDesc>\r\n\t\t\t<PurchaseCost>6.00</PurchaseCost>\r\n\t\t\t<PurchaseTaxCodeRef>\r\n\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t</PurchaseTaxCodeRef>\r\n\t\t\t<COGSAccountRef>\r\n\t\t\t\t<FullName>Cost Of Goods Sold</FullName>\r\n\t\t\t</COGSAccountRef>\r\n\t\t\t<AssetAccountRef>\r\n\t\t\t\t<FullName>Inventory Asset</FullName>\r\n\t\t\t</AssetAccountRef>\r\n\t\t\t<QuantityOnHand>30</QuantityOnHand>\r\n\t\t</ItemInventoryAdd>\r\n\t</ItemInventoryAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Invoice(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<InvoiceAddRq requestID=\"1\">\r\n\t\t<InvoiceAdd>\r\n\t\t\t<CustomerRef>\r\n\t\t\t\t<FullName>Bondi Development Group</FullName>\r\n\t\t\t</CustomerRef>\r\n\t\t\t<TxnDate>2018-09-12</TxnDate>\r\n\t\t\t<RefNumber>INV-1001</RefNumber>\r\n\t\t\t<InvoiceLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<FullName>light saber</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Desc>3 x Light Sabers - 1 x Blue, 1 x Green, 1 x Yellow</Desc>\r\n\t\t\t\t<Quantity>3</Quantity>\r\n\t\t\t\t<Rate>150.00</Rate>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</InvoiceLineAdd>\r\n\t\t</InvoiceAdd>\r\n\t</InvoiceAddRq>\r\n</QBXMLMsgsRq></QBXML>";
                    break;
                case "POST Estimate(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<EstimateAddRq requestID=\"1\">\r\n\t\t<EstimateAdd>\r\n\t\t\t<CustomerRef>\r\n\t\t\t\t<FullName>Bondi Development Group</FullName>\r\n\t\t\t</CustomerRef>\r\n\t\t\t<TxnDate>2018-09-12</TxnDate>\r\n\t\t\t<RefNumber>EST-1001</RefNumber>\r\n\t\t\t<EstimateLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<FullName>light saber</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Desc>3 x Light Sabers - 1 x Blue, 1 x Green, 1 x Yellow</Desc>\r\n\t\t\t\t<Quantity>3</Quantity>\r\n\t\t\t\t<Rate>150.00</Rate>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</EstimateLineAdd>\r\n\t\t</EstimateAdd>\r\n\t</EstimateAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST SalesOrder(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<SalesOrderAddRq requestID=\"1\">\r\n\t\t<SalesOrderAdd>\r\n\t\t\t<CustomerRef>\r\n\t\t\t\t<FullName>Bondi Development Group</FullName>\r\n\t\t\t</CustomerRef>\r\n\t\t\t<TxnDate>2018-09-12</TxnDate>\r\n\t\t\t<RefNumber>SO-1001</RefNumber>\r\n\t\t\t<SalesOrderLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<FullName>light saber</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Desc>3 x Light Sabers - 1 x Blue, 1 x Green, 1 x Yellow</Desc>\r\n\t\t\t\t<Quantity>3</Quantity>\r\n\t\t\t\t<Rate>150.00</Rate>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</SalesOrderLineAdd>\r\n\t\t</SalesOrderAdd>\r\n\t</SalesOrderAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST SalesReceipt(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<SalesReceiptAddRq requestID=\"1\">\r\n\t\t<SalesReceiptAdd>\r\n\t\t\t<CustomerRef>\r\n\t\t\t\t<FullName>Bondi Development Group</FullName>\r\n\t\t\t</CustomerRef>\r\n\t\t\t<TxnDate>2018-09-12</TxnDate>\r\n\t\t\t<RefNumber>RCT-1001</RefNumber>\r\n\t\t\t<SalesReceiptLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<FullName>light saber</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Desc>3 x Light Sabers - 1 x Blue, 1 x Green, 1 x Yellow</Desc>\r\n\t\t\t\t<Quantity>3</Quantity>\r\n\t\t\t\t<Rate>150.00</Rate>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</SalesReceiptLineAdd>\r\n\t\t</SalesReceiptAdd>\r\n\t</SalesReceiptAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST CreditMemo(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CreditMemoAddRq requestID=\"1\">\r\n\t\t<CreditMemoAdd>\r\n\t\t\t<CustomerRef>\r\n\t\t\t\t<FullName>Bondi Development Group</FullName>\r\n\t\t\t</CustomerRef>\r\n\t\t\t<TxnDate>2018-09-12</TxnDate>\r\n\t\t\t<RefNumber>ADJ-1001</RefNumber>\r\n\t\t\t<CreditMemoLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<FullName>light saber</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Desc>3 x Light Sabers - 1 x Blue, 1 x Green, 1 x Yellow</Desc>\r\n\t\t\t\t<Quantity>3</Quantity>\r\n\t\t\t\t<Rate>150.00</Rate>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</CreditMemoLineAdd>\r\n\t\t</CreditMemoAdd>\r\n\t</CreditMemoAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ReceivePayment(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError =\"continueOnError\">\r\n\t<ReceivePaymentAddRq requestID=\"1\">\r\n\t\t<ReceivePaymentAdd>\r\n\t\t\t<CustomerRef>\r\n\t\t\t\t<FullName>ICU Investments</FullName>\r\n\t\t\t</CustomerRef>\r\n\t\t\t<TxnDate>2019-06-10</TxnDate>\r\n\t\t\t<RefNumber>Rcv-1001</RefNumber>\r\n\t\t\t<TotalAmount>1.65</TotalAmount>\r\n\t\t\t<PaymentMethodRef>\r\n\t\t\t\t<FullName>Cash</FullName>\r\n\t\t\t</PaymentMethodRef>\r\n\t\t\t<AppliedToTxnAdd>\r\n\t\t\t\t<TxnID>14B8E-1438323542</TxnID>\r\n\t\t\t\t<PaymentAmount>1.65</PaymentAmount>\r\n\t\t\t</AppliedToTxnAdd>\r\n\t\t</ReceivePaymentAdd>\r\n\t</ReceivePaymentAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST PurchaseOrder(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<PurchaseOrderAddRq requestID=\"1\">\r\n\t\t<PurchaseOrderAdd>\r\n\t\t\t<VendorRef> <FullName>Bunnings Warehouse</FullName>\r\n\t\t\t</VendorRef>\r\n\t\t\t<TxnDate>2019-06-14</TxnDate>\r\n\t\t\t<RefNumber>PO-1001</RefNumber>\r\n\t\t\t<DueDate>2019-07-14</DueDate>\r\n\t\t\t<IsTaxIncluded>false</IsTaxIncluded>\r\n\t\t\t<PurchaseOrderLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<FullName>WAL2P2400x2100</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Quantity>10</Quantity>\r\n\t\t\t\t<Rate>500.00</Rate>\r\n\t\t\t\t<Amount>5000.00</Amount>\r\n\t\t\t\t<TaxAmount>500.00</TaxAmount>\r\n\t\t\t\t<CustomerRef>\r\n\t\t\t\t\t<FullName>fred flinstone</FullName>\r\n\t\t\t\t</CustomerRef>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</PurchaseOrderLineAdd>\r\n\t\t</PurchaseOrderAdd>\r\n\t</PurchaseOrderAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Bill(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<BillAddRq requestID=\"1\">\r\n\t\t<BillAdd>\r\n\t\t\t<VendorRef>\r\n\t\t\t\t<ListID>80000022-1210244066</ListID>\r\n\t\t\t\t<FullName>ABC Chartered Accountants</FullName>\r\n\t\t\t</VendorRef>\r\n\t\t\t<APAccountRef>\r\n\t\t\t\t<ListID>8000005C-1210299519</ListID>\r\n\t\t\t\t<FullName>Accounts Payable</FullName>\r\n\t\t\t</APAccountRef>\r\n\t\t\t<TxnDate>2019-09-14</TxnDate>\r\n\t\t\t<DueDate>2019-10-14</DueDate>\r\n\t\t\t<RefNumber>BLL-1002</RefNumber>\r\n\t\t\t<TermsRef>\r\n\t\t\t\t<ListID>80000005-1210234751</ListID>\r\n\t\t\t\t<FullName>Net 30</FullName>\r\n\t\t\t</TermsRef>\r\n\t\t\t<Memo>Bill from ABC Chartered Accountants For Accounting Service Fees</Memo>\r\n\t\t\t<IsTaxIncluded>false</IsTaxIncluded>\r\n\t\t\t<ExpenseLineAdd>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<ListID>80000048-1210241528</ListID>\r\n\t\t\t\t\t<FullName>Professional Fees:Accounting Fees</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t\t<Amount>240.00</Amount>\r\n\t\t\t\t<TaxAmount>24.00</TaxAmount>\r\n\t\t\t\t<Memo>Accounting Fees - Sydney Office - BAS Lodgement QTR1-JUL to SEP 2018</Memo>\r\n\t\t\t\t<ClassRef>\r\n\t\t\t\t\t<ListID>80000001-1210315305</ListID>\r\n\t\t\t\t\t<FullName>Sydney</FullName>\r\n\t\t\t\t</ClassRef>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<ListID>8000000C-1210234750</ListID>\r\n\t\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</ExpenseLineAdd>\r\n\t\t\t<ExpenseLineAdd>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<ListID>80000048-1210241528</ListID>\r\n\t\t\t\t\t<FullName>Professional Fees:Accounting Fees</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t\t<Amount>190.00</Amount>\r\n\t\t\t\t<TaxAmount>19.00</TaxAmount>\r\n\t\t\t\t<Memo>Accounting Fees - Melbourn Office - BAS Lodgement QTR1-JUL to SEP 2018</Memo>\r\n\t\t\t\t<ClassRef>\r\n\t\t\t\t\t<ListID>80000002-1210315310</ListID>\r\n\t\t\t\t\t<FullName>Melbourne</FullName>\r\n\t\t\t\t</ClassRef>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<ListID>8000000C-1210234750</ListID>\r\n\t\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</ExpenseLineAdd>\r\n\t\t</BillAdd>\r\n\t</BillAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST ItemReceipt(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<ItemReceiptAddRq requestID=\"1\">\r\n\t\t<ItemReceiptAdd>\r\n\t\t\t<VendorRef>\r\n\t\t\t\t<ListID>8000002A-1210244067</ListID>\r\n\t\t\t\t<FullName>Bunnings Warehouse</FullName>\r\n\t\t\t</VendorRef>\r\n\t\t\t<APAccountRef>\r\n\t\t\t\t<ListID>8000005C-1210299519</ListID>\r\n\t\t\t\t<FullName>Accounts Payable</FullName>\r\n\t\t\t</APAccountRef>\r\n\t\t\t<TxnDate>2018-10-22</TxnDate>\r\n\t\t\t<RefNumber>ITMRCT-1001</RefNumber>\r\n\t\t\t<Memo>Item Receipt From Bunnings Warehouse</Memo>\r\n\t\t\t<ItemLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<ListID>80000084-1210592735</ListID>\r\n\t\t\t\t\t<FullName>WW900X900</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Quantity>5</Quantity>\r\n\t\t\t\t<Cost>208.00</Cost>\r\n\t\t\t\t<Amount>1040.00</Amount>\r\n\t\t\t\t<TaxAmount>104.00</TaxAmount>\r\n\t\t\t\t<ClassRef>\r\n\t\t\t\t\t<ListID>80000001-1210315305</ListID>\r\n\t\t\t\t\t<FullName>Sydney</FullName>\r\n\t\t\t\t</ClassRef>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<ListID>8000000C-1210234750</ListID>\r\n\t\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</ItemLineAdd>\r\n\t\t</ItemReceiptAdd>\r\n\t</ItemReceiptAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST VendorCredit(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<VendorCreditAddRq requestID=\"1\">\r\n\t\t<VendorCreditAdd>\r\n\t\t\t<VendorRef>\r\n\t\t\t\t<ListID>8000002A-1210244067</ListID>\r\n\t\t\t\t<FullName>Bunnings Warehouse</FullName>\r\n\t\t\t</VendorRef>\r\n\t\t\t<APAccountRef>\r\n\t\t\t\t<ListID>8000005C-1210299519</ListID>\r\n\t\t\t\t<FullName>Accounts Payable</FullName>\r\n\t\t\t</APAccountRef>\r\n\t\t\t<TxnDate>2018-10-18</TxnDate>\r\n\t\t\t<RefNumber>BLLCRT-1001</RefNumber>\r\n\t\t\t<Memo>Bill Credit for Bunnings Warehouse</Memo>\r\n\t\t\t<ItemLineAdd>\r\n\t\t\t\t<ItemRef>\r\n\t\t\t\t\t<ListID>80000083-1210592735</ListID>\r\n\t\t\t\t\t<FullName>WW900x600</FullName>\r\n\t\t\t\t</ItemRef>\r\n\t\t\t\t<Quantity>2</Quantity>\r\n\t\t\t\t<Cost>188.00</Cost>\r\n\t\t\t\t<Amount>376.00</Amount>\r\n\t\t\t\t<TaxAmount>37.60</TaxAmount>\r\n\t\t\t\t<ClassRef>\r\n\t\t\t\t\t<ListID>80000001-1210315305</ListID>\r\n\t\t\t\t\t<FullName>Sydney</FullName>\r\n\t\t\t\t</ClassRef>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<ListID>8000000C-1210234750</ListID>\r\n\t\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</ItemLineAdd>\r\n\t\t</VendorCreditAdd>\r\n\t</VendorCreditAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST BillPaymentCheck(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<BillPaymentCheckAddRq requestID=\"1\">\r\n\t\t<BillPaymentCheckAdd>\r\n\t\t\t<PayeeEntityRef>\r\n\t\t\t\t<ListID>80000042-1210244067</ListID>\r\n\t\t\t</PayeeEntityRef>\r\n\t\t\t<APAccountRef>\r\n\t\t\t\t<ListID>8000005C-1210299519</ListID>\r\n\t\t\t\t<FullName>Accounts Payable</FullName>\r\n\t\t\t</APAccountRef>\r\n\t\t\t<TxnDate>2009-06-08</TxnDate>\r\n\t\t\t<BankAccountRef>\r\n\t\t\t\t<ListID>80000038-1210240392</ListID>\r\n\t\t\t\t<FullName>Bank Cheque Account</FullName>\r\n\t\t\t</BankAccountRef>\r\n\t\t\t<IsToBePrinted>true</IsToBePrinted>\r\n\t\t\t<Memo>Bill Payment to A.James glass and aluminium</Memo>\r\n\t\t\t<AppliedToTxnAdd>\r\n\t\t\t\t<TxnID>E851-1301281443</TxnID>\r\n\t\t\t\t<PaymentAmount>300.00</PaymentAmount>\r\n\t\t\t</AppliedToTxnAdd>\r\n\t\t</BillPaymentCheckAdd>\r\n\t</BillPaymentCheckAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Check(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<CheckAddRq requestID=\"1\">\r\n\t\t<CheckAdd>\r\n\t\t\t<AccountRef>\r\n\t\t\t\t<ListID>80000038-1210240392</ListID>\r\n\t\t\t\t<FullName>Bank Cheque Account</FullName>\r\n\t\t\t</AccountRef>\r\n\t\t\t<PayeeEntityRef>\r\n\t\t\t\t<ListID>80000022-1210244066</ListID>\r\n\t\t\t\t<FullName>ABC Chartered Accountants</FullName>\r\n\t\t\t</PayeeEntityRef>\r\n\t\t\t<RefNumber>CHK-1001</RefNumber>\r\n\t\t\t<TxnDate>2018-10-18</TxnDate>\r\n\t\t\t<Memo>Check To ABC Chartered Accountants For Accounting Service Fees</Memo>\r\n\t\t\t<IsToBePrinted>true</IsToBePrinted>\r\n\t\t\t<IsTaxIncluded>false</IsTaxIncluded>\r\n\t\t\t<ExpenseLineAdd>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<ListID>80000048-1210241528</ListID>\r\n\t\t\t\t\t<FullName>Professional Fees:Accounting Fees</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t\t<Amount>240.00</Amount>\r\n\t\t\t\t<TaxAmount>24.00</TaxAmount>\r\n\t\t\t\t<Memo>Accounting Fees - Sydney Office - BAS Lodgement QTR1-JUL to SEP 2018</Memo>\r\n\t\t\t\t<ClassRef>\r\n\t\t\t\t\t<ListID>80000001-1210315305</ListID>\r\n\t\t\t\t\t<FullName>Sydney</FullName>\r\n\t\t\t\t</ClassRef>\r\n\t\t\t\t<SalesTaxCodeRef>\r\n\t\t\t\t\t<ListID>8000000C-1210234750</ListID>\r\n\t\t\t\t\t<FullName>NCG</FullName>\r\n\t\t\t\t</SalesTaxCodeRef>\r\n\t\t\t</ExpenseLineAdd>\r\n\t\t</CheckAdd>\r\n\t</CheckAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST JournalEntry(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<JournalEntryAddRq requestID=\"1\">\r\n\t\t<JournalEntryAdd>\r\n\t\t\t<TxnDate>2019-06-05</TxnDate>\r\n\t\t\t<RefNumber>GJ001</RefNumber>\r\n\t\t\t<IsAdjustment>false</IsAdjustment>\r\n\t\t\t<JournalCreditLine>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<ListID>80000041-1210240662</ListID>\r\n\t\t\t\t\t<FullName>Accounts Receivable</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t\t<Amount>100.00</Amount>\r\n\t\t\t\t<Memo>Accounts Receivable Credit Line</Memo>\r\n\t\t\t\t<EntityRef>\r\n\t\t\t\t\t<ListID>80000009-1210244039</ListID>\r\n\t\t\t\t\t<FullName>ICU Investments</FullName>\r\n\t\t\t\t</EntityRef>\r\n\t\t\t\t<ItemSalesTaxRef>\r\n\t\t\t\t\t<ListID>80000009-1210234750</ListID>\r\n\t\t\t\t\t<FullName>GST</FullName>\r\n\t\t\t\t</ItemSalesTaxRef>\r\n\t\t\t</JournalCreditLine>\r\n\t\t\t<JournalDebitLine>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<ListID>80000038-1210240392</ListID>\r\n\t\t\t\t\t<FullName>Bank Cheque Account</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t\t<Amount>110.00</Amount>\r\n\t\t\t\t<Memo>CBank Cheque Account Debit Line</Memo>\r\n\t\t\t</JournalDebitLine>\r\n\t\t</JournalEntryAdd>\r\n\t</JournalEntryAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
                case "POST Deposit(Add)":
                    _instance.RahBody = "<?xml version=\"1.0\"?>\r\n<?qbxml version=\"6.1\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"continueOnError\">\r\n\t<DepositAddRq requestID=\"1\">\r\n\t\t<DepositAdd>\r\n\t\t\t<TxnDate>2019-06-10</TxnDate>\r\n\t\t\t<DepositToAccountRef>\r\n\t\t\t\t<FullName>Bank Cheque Account</FullName>\r\n\t\t\t</DepositToAccountRef>\r\n\t\t\t<Memo>Deposit from Carlton Security</Memo>\r\n\t\t\t<DepositLineAdd>\r\n\t\t\t\t<EntityRef>\r\n\t\t\t\t\t<FullName>Carlton Security</FullName>\r\n\t\t\t\t</EntityRef>\r\n\t\t\t\t<AccountRef>\r\n\t\t\t\t\t<FullName>Contracting Income</FullName>\r\n\t\t\t\t</AccountRef>\r\n\t\t\t\t<Amount>370.00</Amount>\r\n\t\t\t</DepositLineAdd>\r\n\t\t</DepositAdd>\r\n\t</DepositAddRq>\r\n\r\n\t<DepositAddRq requestID =\"2\">\r\n\t\t<DepositAdd>\r\n\t\t\t<TxnDate>2019-06-10</TxnDate>\r\n\t\t\t<DepositToAccountRef>\r\n\t\t\t\t<FullName>Bank Cheque Account</FullName>\r\n\t\t\t</DepositToAccountRef>\r\n\t\t\t<Memo>Deposit of Receive Payment</Memo>\r\n\t\t\t<DepositLineAdd>\r\n\t\t\t\t<PaymentTxnID>14B22-1415054122</PaymentTxnID>\r\n\t\t\t\t<PaymentTxnLineID>14B25-1415054122</PaymentTxnLineID>\r\n\t\t\t</DepositLineAdd>\r\n\t\t</DepositAdd>\r\n\t</DepositAddRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";
                    break;
            }

        }

        //Call to RAH Get Supported Versions End Point
        public void GetSupportedVersions()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, RahSubscriptionKey);
                var response = httpClient.GetAsync($"{Globals.ApiBaseUrl}{RahApiSegment}{RahSupportedVersions}").Result;
                Globals.RahStatus = response.StatusCode.ToString();
                string responseText = response.Content.ReadAsStringAsync().Result;
                JArray parsedArray = JArray.Parse(responseText);
                foreach (JObject parsedObject in parsedArray.Children<JObject>())
                {
                    foreach (JProperty parsedProperty in parsedObject.Properties())
                    {
                        if (parsedProperty.Name.Equals("CountryVersion"))
                        {
                            _instance.VersionsCombo = parsedProperty.Value.ToString();
                            using (MySqlConnection mySqlConnection = new MySqlConnection())
                            {
                                mySqlConnection.ConnectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", Server, Port, User, Password, Database, SSLMode);
                                mySqlConnection.Open();
                                string sqlQuery = "INSERT INTO rahsupportedversions VALUES (@countryVersion) " +
                                "ON DUPLICATE KEY UPDATE countryVersion=@countryVersion";
                                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, mySqlConnection);
                                mySqlCommand.Parameters.AddWithValue("@countryVersion", parsedProperty.Value.ToString());
                                MySqlDataReader _mySqlDataReader = mySqlCommand.ExecuteReader();
                                mySqlConnection.Close();
                            }
                        }
                    }
                }
            }
        }

        //Save Hosted File Details
        public void SaveHostedFileDetails()
        {
            FilePath = _instance.FilePath;
            FileUserName = _instance.FileUser;
            FilePassword = _instance.FilePassword;
            CountryVersion = _instance.VersionsCombo;
            using (MySqlConnection mySqlConnection = new MySqlConnection())
            {
                
                mySqlConnection.ConnectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", Server, Port, User, Password, Database, SSLMode);
                mySqlConnection.Open();
                string sqlQuery = "UPDATE rahfiledetails SET filepath=@filepath, fileuser=@fileuser, filepassword=@filepassword" +
                    ", countryversion=@countryversion ";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, mySqlConnection);
                mySqlCommand.Parameters.AddWithValue("@filepath", FilePath);
                mySqlCommand.Parameters.AddWithValue("@fileuser", FileUserName);
                mySqlCommand.Parameters.AddWithValue("@filepassword", FilePassword);
                mySqlCommand.Parameters.AddWithValue("@countryversion", CountryVersion);
                MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                mySqlConnection.Close();
            }
            MessageBox.Show("Your file details have been saved!", "Success!", MessageBoxButtons.OK);
        }
    }
}
