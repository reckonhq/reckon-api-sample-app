﻿
namespace Reckon_API_Sample_App
{
    //Class holding globals
    class Globals
    {
        public static string SelectedApp { get; set; }
        public static string RahStatus { get; set; }
        public static string R1Status { get; set; }
        public static bool RahRevoked { get; set; }
        public static bool R1Revoked { get; set; }
        public static string SelectedBook { get; set; }
        public static string SelectedFunction { get; set; }
        public static string SelectedFunctionType { get; set; }
        public static string SavedBook { get; set; }
        public static string SavedBookName { get; set; }
        public static string SavedFunction { get; set; }
        public static string SavedFunctionName { get; set; }
        public static string SubscriptionKeyType { get; } = "Ocp-Apim-Subscription-Key";
        public static string ApiBaseUrl { get; } = "https://api.reckon.com";
    }
}
