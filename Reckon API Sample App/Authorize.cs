﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Configuration;


namespace Reckon_API_Sample_App
{
    class Authorize
    {
        public string Access_token { get; set; }
        public string Expires_in { get; set; }
        public string Token_type { get; set; }
        public string Refresh_token { get; set; }
        
        public static string AuthCode { get; set; }
        public static string AccessToken { get; set; }
        public static string RefreshToken { get; set; }
        public static string TokenType { get; set; }
        public static string TokenExpiry { get; set; }

        public static string ClientId { get; } = ConfigurationManager.AppSettings["clientId"].ToString();
        public static string ClientSecret { get; } = ConfigurationManager.AppSettings["clientSecret"].ToString();
        public static string RedirectUrl { get; } = ConfigurationManager.AppSettings["redirectUrl"].ToString();
        public static string IdentityServerUrl { get; } = "https://identity.reckon.com";
        public static string TokenSegment { get; } = "/connect/token";

        public static string Server { get; } = ConfigurationManager.AppSettings["server"].ToString();
        public static string Database { get; } = ConfigurationManager.AppSettings["database"].ToString();
        public static string User { get; } = ConfigurationManager.AppSettings["user"].ToString();
        public static string Password { get; } = ConfigurationManager.AppSettings["password"].ToString();
        public static string Port { get; } = ConfigurationManager.AppSettings["port"].ToString();
        public static string SSLMode { get; } = ConfigurationManager.AppSettings["sslMode"].ToString();

        //Encode client id and client secret in base64
        public string Base64Encoding()
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes($"{ ClientId }:{ ClientSecret }"));
        }

        //Make token requests and store results in MySQL database.  Used for access token and refresh token requests
        public void GetToken(string grantType, string bodyKey, string bodyValue, string selectedApp)
        {
            if (Globals.SelectedApp == "RAH") selectedApp = "rahauth"; //Use rahauth database table
            else selectedApp = "r1auth"; //Use r1auth database table
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Base64Encoding());
                var content = new StringContent($"grant_type={grantType}&{bodyKey}={bodyValue}&redirect_uri={RedirectUrl}",
                    Encoding.UTF8, "application/x-www-form-urlencoded");
                var response = httpClient.PostAsync($"{IdentityServerUrl}{TokenSegment}", content).Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                var result = JsonConvert.DeserializeObject<Authorize>(responseText);
                AccessToken = result.Access_token;
                RefreshToken = result.Refresh_token;
                TokenExpiry = result.Expires_in;
                TokenType = result.Token_type;
            }
            using (MySqlConnection mySqlConnection = new MySqlConnection())
            {
                mySqlConnection.ConnectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", Server, Port, User, Password, Database, SSLMode);
                mySqlConnection.Open();
                string sqlQuery = "UPDATE " + selectedApp + " SET accessToken=@accessToken, refreshToken=@refreshToken, tokenExpiry=@tokenExpiry" +
                    ", tokenType=@tokenType, authTime=@dateTimeString ";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, mySqlConnection);
                mySqlCommand.Parameters.AddWithValue("@accessToken", AccessToken);
                mySqlCommand.Parameters.AddWithValue("@refreshToken", RefreshToken);
                mySqlCommand.Parameters.AddWithValue("@tokenExpiry", TokenExpiry);
                mySqlCommand.Parameters.AddWithValue("tokenType", TokenType);
                mySqlCommand.Parameters.AddWithValue("@dateTimeString", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                mySqlConnection.Close();
            }
        }

        //Make standard token request by calling GetToken() with selectedApp argument
        public void AccessTokenRequest(string selectedApp)
        {
            GetToken("authorization_code", "code", AuthCode, selectedApp);
            AuthCode = "";
        }

        //Make refresh token request by calling GetToken() with selectedApp argument
        public void RefreshTokenRequest(string selectedApp)
        {
            GetToken("refresh_token", "refresh_token", RefreshToken, selectedApp);
        }

        //Clear app and MySql Database of tokens
        public void ClearAuth(string selectedApp)
        {
            using (MySqlConnection mySqlConnection = new MySqlConnection())
            {
                mySqlConnection.ConnectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", Server, Port, User, Password, Database, SSLMode);
                mySqlConnection.Open();
                string sqlQuery = "UPDATE " + selectedApp + " SET accessToken=@accessToken, refreshToken=@refreshToken, tokenExpiry=@tokenExpiry" +
                    ", tokenType=@tokenType, authTime=@dateTimeString ";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, mySqlConnection);
                mySqlCommand.Parameters.AddWithValue("@accessToken", "11111");
                mySqlCommand.Parameters.AddWithValue("@refreshToken", "11111");
                mySqlCommand.Parameters.AddWithValue("@tokenExpiry", "11111");
                mySqlCommand.Parameters.AddWithValue("tokenType", "Bearer");
                mySqlCommand.Parameters.AddWithValue("@dateTimeString", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                MySqlDataReader _mySqlDataReader = mySqlCommand.ExecuteReader();
                mySqlConnection.Close();
            }
            AccessToken = null;
            RefreshToken = null;
            AuthCode = null;
        }

        //Check if app has been authorized. Admin panel displays "Connected" if true.
        public bool IsConnected()
        {
            if (!string.IsNullOrEmpty(AccessToken)) return true;
            else return false;
        }
    }
}
