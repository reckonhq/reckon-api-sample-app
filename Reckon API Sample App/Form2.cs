﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Reckon_API_Sample_App
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        public static string ClientId { get; } = ConfigurationManager.AppSettings["clientId"].ToString(); 
        public static string RedirectUrl { get; } = ConfigurationManager.AppSettings["redirectUrl"].ToString();
        public static string IdentityServerUrl { get; } = "https://identity.reckon.com";
        public static string AuthorizeSegment { get; } = "/connect/authorize";
        public static string TokenSegment { get; } = "/connect/token";
        public static string RevokeSegment { get; } = "/permissions";

        private void Form2_Load(object sender, EventArgs e)
        {
            if (Globals.RahRevoked || Globals.R1Revoked)
            {
                WebBrowser.Navigate(string.Format("{0}{1}", IdentityServerUrl, RevokeSegment));
                Globals.RahRevoked = false;
                Globals.R1Revoked = false;
            }
            else
            {
                WebBrowser.Navigate(string.Format("{0}{1}{2}", IdentityServerUrl, AuthorizeSegment, 
                    string.Format("?client_id={0}&response_type=code&scope=read+write+offline_access&redirect_uri={1}&state=random_state&nonce=random_nonce",
                    ClientId, RedirectUrl)));
            }
        }

        //Start of WebBrowser Control Functions

        private void BackButton_Click(object sender, EventArgs e)
        {
            WebBrowser.GoBack();
        }

        private void ForwardButton_Click(object sender, EventArgs e)
        {
            WebBrowser.GoForward();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            WebBrowser.GoSearch();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            WebBrowser.Refresh();
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            AddressBar.Text = WebBrowser.Url.ToString();
        }

        //End of WebBrowser control functions


        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            //When Form2 closes, open a new Form1
            Form1 form1 = new Form1();
            form1.Show();
            Dispose();
        }

        //Get Authorization Code from Address Bar URL
        public string getAuthCode()
        {
            Regex regex = new Regex(@"code=(\S+?){32}?");
            Match match = regex.Match(WebBrowser.Url.ToString());
            string myMatch = match.ToString();
            return myMatch.Remove(0, 5);
        }

        //When address bar text changes, check for authorization code
        private void AddressBar_TextChanged(object sender, EventArgs e)
        {
            if (AddressBar.Text.Contains("code=") == true)
            {
                //When authcode is available assign it to AuthCode to be used in Token Request
                Authorize.AuthCode = getAuthCode();
                Close();
            }
        }

        
    }
}
