﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Threading;

namespace Reckon_API_Sample_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //RAH App Control Properties
        public string FilePath
        {
            get { return FilePathText.Text; }
            set { FilePathText.Text = value; }
        }

        public string FileUser
        {
            get { return FileUserText.Text; }
            set { FileUserText.Text = value; }
        }

        public string FilePassword
        {
            get { return FilePwdText.Text; }
            set { FilePwdText.Text = value; }
        }

        public string VersionsCombo
        {
            get { return VersionCombo.Text; }
            set { VersionCombo.Items.Add(value); }
        }

        public string RahFunctionsComboBox
        {
            get { return RahFunctionsCombo.Text; }
        }

        public string RahBody
        {
            get { return RahBodyText.Text; }
            set { RahBodyText.Text = value; }
        }

        public string RahResp
        {
            get { return RahRespText.Text; }
            set { RahRespText.Text = value; }
        }

        public string RahGuid
        {
            get { return RahRqIdText.Text; }
            set { RahRqIdText.Text = value; }
        }

        public void ClearRahRespText()
        {
            RahRespText.Clear();
        }

        public string RahGetIdButtonText
        {
            set { RahGetIdButton.Text = value; }
        }

        public Color RahGetIdButtonColor
        {
            set { RahGetIdButton.BackColor = value; }
        }

        public bool RahGetIdButtonEnabled
        {
            set { RahGetIdButton.Enabled = value; }
        }

        public string RahPostButtonText
        {
            set { RahPostButton.Text = value; }
        }

        public Color RahPostButtonColor
        {
            set { RahPostButton.BackColor = value; }
        }

        public bool RahPostButtonEnabled
        {
            set { RahPostButton.Enabled = value; }
        }

        //Reckon One App Control Properties
        public void BooksComboboxAddItem(DataSet dataset)
        {
            BooksCombo.DataSource = dataset.Tables[0];
            BooksCombo.DisplayMember = "BookName";
            BooksCombo.ValueMember = "BookId";
            BooksCombo.SelectedIndex = -1;
        }

        public string R1BooksCombo
        {
            get { return BooksCombo.Text; }
        }

        public string FunctionsCombo
        {
            get { return FunctionCombo.Text; }
        }

        public string R1GetId
        {
            get { return GetIdText.Text; }
            set { GetIdText.Text = value; }
        }

        public string R1DeleteId
        {
            get { return DeleteIdText.Text; }
            set { DeleteIdText.Text = value; }
        }

        public string R1Body
        {
            get { return R1PostText.Text; }
            set { R1PostText.Text = value; }
        }

        public string R1Put
        {
            get { return R1PutText.Text; }
            set { R1PutText.Text = value; }
        }

        public string R1Resp
        {
            get { return R1ResponseText.Text; }
            set { R1ResponseText.Text = value; }
        }

        public string R1GoButtonText
        {
            set { R1GoButton.Text = value; }
        }

        public Color R1GoButtonBackColor
        {
            set { R1GoButton.BackColor = value; }
        }

        public bool R1GoButtonEnabled
        {
            set { R1GoButton.Enabled = value; }
        }

        public string R1GetIdButtonText
        {
            set { GetIdButton.Text = value; }
        }

        public Color R1GetIdButtonBackColor
        {
            set { GetIdButton.BackColor = value; }
        }

        public bool R1GetIdButtonEnabled
        {
            set { GetIdButton.Enabled = value; }
        }

        public string R1DeleteIdButtonText
        {
            set { DeleteIdButton.Text = value; }
        }

        public Color R1DeleteIdButtonBackColor
        {
            set { DeleteIdButton.BackColor = value; }
        }

        public bool R1DeleteIdButtonEnabled
        {
            set { DeleteIdButton.Enabled = value; }
        }

        public string R1PostButtonText
        {
            set { R1PostButton.Text = value; }
        }

        public Color R1PostButtonBackColor
        {
            set { R1PostButton.BackColor = value; }
        }

        public bool R1PostButtonEnabled
        {
            set { R1PostButton.Enabled = value; }
        }

        public string R1PutButtonText
        {
            set { R1PutButton.Text = value; }
        }

        public Color R1PutButtonBackColor
        {
            set { R1PutButton.BackColor = value; }
        }

        public bool R1PutButtonEnabled
        {
            set { R1PutButton.Enabled = value; }
        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
            HomePanel.Hide();
            RahConnPanel.Hide();
            R1ConnPanel.Hide();
            R1AppPanel.Hide();
            RahAppPanel.Hide();
            FilePathText.Text = ReckonAccountsHosted.FilePath;
            FileUserText.Text = ReckonAccountsHosted.FileUserName;
            FilePwdText.Text = ReckonAccountsHosted.FilePassword;
            VersionCombo.Text = ReckonAccountsHosted.CountryVersion;
            if (string.IsNullOrEmpty(Globals.SelectedApp) && string.IsNullOrEmpty(Authorize.AuthCode) && string.IsNullOrEmpty(Authorize.AccessToken))
            {
                HomePanel.Show();
            }
            else
            {
                if (Globals.SelectedApp == "R1")
                {
                    if (string.IsNullOrEmpty(Authorize.AuthCode) && string.IsNullOrEmpty(Authorize.AccessToken))
                        R1ConnPanel.Show();

                    else if (!string.IsNullOrEmpty(Authorize.AuthCode))
                    {
                        Authorize authorize = new Authorize();
                        try
                        {
                            authorize.AccessTokenRequest(Globals.SelectedApp);
                        }
                        catch (MySqlException ex)
                        {
                            MessageBox.Show("The application has encountered a MySQL Error. " +
                                "\r\nEnsure your MySQL Server is started and try running the application again. " +
                                "\r\n\r\nClosing Application", "MySql Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Console.WriteLine($"\r\nR1 Authorization Failed. \r\nThe following MySql exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                            Console.ReadLine();
                            Application.Exit();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("An unexpected error has occured. \r\n\r\nClosing Application.", "Unexpected Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Console.WriteLine($"\r\nR1 Authorization Failed. \r\nThe following exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                            Console.ReadLine();
                            Application.Exit();
                        }

                        if (string.IsNullOrEmpty(Authorize.AuthCode) && !string.IsNullOrEmpty(Authorize.AccessToken))
                        {
                            ReckonOne r1 = new ReckonOne(this);
                            r1.R1Hearbeat();
                            try
                            {
                                r1.GetBooks();
                            }
                            catch (JsonReaderException ex)
                            {
                                MessageBox.Show("Unable to retreive your list of books from the API. \r\nEnsure you login with your \"User Name\" " +
                                    "and not your \"User ID\" when connecting to Reckon One. \r\n\r\nClosing Application.", "Get CashBooks Failed",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Console.WriteLine($"\r\nR1 GetBooks Failed. \r\nThe following JsonReader exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                                Console.ReadLine();
                                Application.Exit();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("An unexpected error has occured. \r\n\r\nClosing Application.", "Unexpected Error",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Console.WriteLine($"\r\nR1 GetBooks Failed. \r\nThe following exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                                Console.ReadLine();
                                Application.Exit();
                            }
                            R1AppPanel.Show();
                            GetIdPanel.Hide();
                            DeleteIdPanel.Hide();
                            R1PostPanel.Hide();
                            R1PostButton.Hide();
                            R1PutPanel.Hide();
                            R1PutButton.Hide();
                            R1ResponsePanel.Hide();
                            R1ClearButton.Hide();
                            if (!string.IsNullOrEmpty(Globals.SavedBook))
                            {
                                BooksCombo.Text = Globals.SavedBookName;
                                if (!string.IsNullOrEmpty(Globals.SavedFunction)) FunctionCombo.Text = Globals.SavedFunctionName;
                            }
                            if (authorize.IsConnected()) R1ApConnLabel.Text = "Connected!";
                            if (Globals.R1Status == "OK") R1ApHbStatus.Text = "OK!";
                            else
                            {
                                R1ApHbStatus.Text = "NO!";
                                R1HearbeatMessageBox();
                            }
                        }
                    }
                }
                else if (Globals.SelectedApp == "RAH")
                {
                    if (string.IsNullOrEmpty(Authorize.AuthCode) && string.IsNullOrEmpty(Authorize.AccessToken))
                        RahConnPanel.Show();
                    else if (!(string.IsNullOrEmpty(Authorize.AuthCode)))
                    {
                        Authorize authorize = new Authorize();
                        try
                        {
                            authorize.AccessTokenRequest(Globals.SelectedApp);
                        }
                        catch (MySqlException ex)
                        {
                            MessageBox.Show("The application has encountered a MySQL Error. " +
                                "\r\nEnsure your MySQL Server is started and try running the application again. " +
                                "\r\n\r\nClosing Application", "MySql Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Console.WriteLine($"\r\nRAH Authorization Failed. \r\nThe following MySql exception occured:\r\n{ ex.Message }\r\n\r\n{ ex.StackTrace }");
                            Console.ReadLine();
                            Application.Exit();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("An unexpected error has occured. \r\n\r\nClosing Application.", "Unexpected Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Console.WriteLine($"\r\nRAH Authorization Failed. \r\nThe following exception occured:\r\n{ ex.Message }\r\n\r\n{ ex.StackTrace }");
                            Console.ReadLine();
                            Application.Exit();
                        }

                        if (string.IsNullOrEmpty(Authorize.AuthCode) && !string.IsNullOrEmpty(Authorize.AccessToken))
                        {
                            ReckonAccountsHosted rah = new ReckonAccountsHosted(this);
                            rah.RAHHearbeat();
                            try
                            {
                                rah.GetSupportedVersions();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("An unexpected error has occured. \r\n\r\nClosing Application.", "Unexpected Error",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Console.WriteLine($"\r\nRAH GetSupportedVersions Failed. \r\nThe following exception occured:\r\n{ ex.Message }\r\n\r\n{ ex.StackTrace }");
                                Console.ReadLine();
                                Application.Exit();
                            }
                            RahAppPanel.Show();
                            RahBodyPanel.Hide();
                            RahPostButton.Hide();
                            RahRespPanel.Hide();
                            RahClearButton.Hide();
                            RahRqIdPanel.Hide();
                            RahGetIdButton.Hide();
                            if (authorize.IsConnected()) RahApConnLabel.Text = "Connected!";
                            if (Globals.RahStatus == "OK") RahApHbStatus.Text = "OK!";
                            else
                            {
                                RahApHbStatus.Text = "NO!";
                                RAHHearbeatMessageBox();
                            }
                        }
                    }
                }
                else HomePanel.Show();
            }
        }

        //Message to display if Heartbeat request returns as failed
        public void RAHHearbeatMessageBox()
        {
            if (MessageBox.Show("RAH Hearbeat is down.  Would you like to view the API Status page?", "RAH Heartbeat", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start("http://apistatus.reckon.com/");
            }
        }

        //Message to display if Heartbeat request returns as failed
        public void R1HearbeatMessageBox()
        {
            if (MessageBox.Show("R1 Hearbeat is down.  Would you like to view the API Status page?", "R1 Heartbeat", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start("http://apistatus.reckon.com/");
            }
        }

        //Reckon Accounts Hosted App Selected
        private void RahAppSelectButton_Click(object sender, EventArgs e)
        {
            HomePanel.Hide();
            RahConnPanel.Show();
            Globals.SelectedApp = "RAH";
        }

        //Reckon One App Selected
        private void R1AppSelectButton_Click(object sender, EventArgs e)
        {
            HomePanel.Hide();
            R1ConnPanel.Show();
            Globals.SelectedApp = "R1";
        }

        //Reckon Accounts Hosted Connect Button Clicked
        private void RahConnButton_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
            RahAppPanel.Show();
        }

        //Reckon One Connect Button Clicked
        private void R1ConnButton_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
            R1AppPanel.Show();
        }

        //Home Icon on RahConnPanel Clicked
        private void RahConnHome_Click(object sender, EventArgs e)
        {
            HomePanel.Show();
            RahConnPanel.Hide();
        }

        //Home Icon on R1ConnPanel Clicked
        private void R1ConnHome_Click(object sender, EventArgs e)
        {
            HomePanel.Show();
            R1ConnPanel.Hide();
        }

        //Home Icon on RahAppPanel Clicked
        private void RahAppHome_Click(object sender, EventArgs e)
        {
            HomePanel.Show();
            RahAppPanel.Hide();
        }

        //Home Icon on R1AppPanel Clicked
        private void R1AppHome_Click(object sender, EventArgs e)
        {
            HomePanel.Show();
            R1AppPanel.Hide();
        }

        //Reckon Accounts Hosted Refresh Token Request Clicked
        private void RahApRfshLabel_Click(object sender, EventArgs e)
        {
            Authorize authorize = new Authorize();
            try
            {
                authorize.RefreshTokenRequest(Globals.SelectedApp);
                if (authorize.IsConnected())
                {
                    MessageBox.Show("Refresh Token Request Successfull!", "Refresh Success!", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Refresh Token Request Failed! Try using \"Auth Again\".", "Refresh Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An unexpected error has occured. \r\nTry using Auth Again instead. \r\nIf the problem persists restart the application.", "Unexpected Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nRAH RefreshTokenRequest Failed. \r\nThe following exception occured:\r\n { ex.Message }\r\n\r\n { ex.StackTrace }");
                Console.ReadLine();
            }
        }

        //Reckon Accounts Hosted Revoke App Permissions
        private void RahApRvkLabel_Click(object sender, EventArgs e)
        {
            Authorize authorize = new Authorize();
            authorize.ClearAuth("rahauth");
            Globals.RahRevoked = true;
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
        }

        //Reckon Accounts Hosted Authorize App Again
        private void RahApAuthLabel_Click(object sender, EventArgs e)
        {
            Authorize.AccessToken = "";
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
            RahAppPanel.Show();
        }

        //Reckon One Refresh Token Request Clicked
        private void R1ApRfshLabel_Click(object sender, EventArgs e)
        {
            Authorize authorize = new Authorize();
            try
            {
                authorize.RefreshTokenRequest(Globals.SelectedApp);
                if (authorize.IsConnected())
                {
                    MessageBox.Show("Refresh Token Request Successfull!", "Refresh Success!", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Refresh Token Request Failed! Try using \"Auth Again\".", "Refresh Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An unexpected error has occured. \r\nTry using Auth Again instead. \r\nIf the problem persists restart the application.", "Unexpected Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nR1 RefreshTokenRequest Failed. \r\nThe following exception occured:\r\n { ex.Message }\r\n\r\n { ex.StackTrace }");
                Console.ReadLine();
            }
        }

        //Reckon One Revoke App Permissions
        private void R1ApRvkLabel_Click(object sender, EventArgs e)
        {
            Authorize authorize = new Authorize();
            authorize.ClearAuth("rahauth");
            Globals.RahRevoked = true;
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
        }

        //Reckon One Authorize App Again
        private void R1ApAuthLabel_Click(object sender, EventArgs e)
        {
            Authorize.AccessToken = "";
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
            R1AppPanel.Show();
        }

        //Save Reckon Accounts Hosted Request Details
        private void FileSaveButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FilePathText.Text) || string.IsNullOrEmpty(FileUserText.Text) || string.IsNullOrEmpty(VersionCombo.Text))
                MessageBox.Show("You must enter a file path, a username and select a version before saving.", "Save Failed",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                ReckonAccountsHosted rah = new ReckonAccountsHosted(this);
                try
                {
                    rah.SaveHostedFileDetails();
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show("The application has encountered a MySQL Error. " +
                        "\r\nEnsure your MySQL Server is started and try saving again. ", "MySql Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nRAH FileSave Failed. \r\nThe following MySql exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An unexpected error has occured. \r\nTry saving again, if the problem persists restart the application.", "Unexpected Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nRAH FileSave Failed. \r\nThe following exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                    Console.ReadLine();
                }
            }
        }


        private void BooksCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BooksCombo.SelectedIndex > 0)
                Globals.SelectedBook = string.Format("/{0}", BooksCombo.SelectedValue.ToString());
        }

        //On Reckon One FunctionsCombo combobox selection change run the FunctionSelect() function
        private void FunctionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReckonOne r1 = new ReckonOne(this);
            r1.FunctionSelect(FunctionCombo.Text);
            if (Globals.SelectedFunctionType == "GET")
            {
                GetIdPanel.Show();
                DeleteIdPanel.Hide();
                R1PostPanel.Hide();
                R1PostButton.Hide();
                R1PutPanel.Hide();
                R1PutButton.Hide();
                R1ResponsePanel.Hide();
                R1ClearButton.Hide();
                R1GoButton.Show();
            }
            else if (Globals.SelectedFunctionType == "DELETE")
            {
                DeleteIdPanel.Show();
                GetIdPanel.Hide();
                R1PostPanel.Hide();
                R1PostButton.Hide();
                R1PutPanel.Hide();
                R1PutButton.Hide();
                R1ResponsePanel.Hide();
                R1ClearButton.Hide();
                R1GoButton.Hide();
            }
            else if (Globals.SelectedFunctionType == "POST")
            {
                R1PostPanel.Show();
                R1PostButton.Show();
                GetIdPanel.Hide();
                DeleteIdPanel.Hide();
                R1PutPanel.Hide();
                R1PutButton.Hide();
                R1ResponsePanel.Hide();
                R1ClearButton.Hide();
                R1GoButton.Hide();
                MessageBox.Show("Ensure you edit the JSON POST request with valid information", "Edit JSON Request", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (Globals.SelectedFunctionType == "PUT")
            {
                R1PutPanel.Show();
                R1PutButton.Show();
                DeleteIdPanel.Hide();
                GetIdPanel.Hide();
                R1PostPanel.Hide();
                R1PostButton.Hide();
                R1ResponsePanel.Hide();
                R1ClearButton.Hide();
                R1GoButton.Hide();
                MessageBox.Show("Ensure you edit the JSON PUT request with valid information", "Edit JSON Request", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(FunctionCombo.Text))
            {
                R1PutPanel.Hide();
                R1PutButton.Hide();
                DeleteIdPanel.Hide();
                GetIdPanel.Hide();
                R1PostPanel.Hide();
                R1PostButton.Hide();
                R1ResponsePanel.Hide();
                R1ClearButton.Hide();
                R1GoButton.Show();
            }
        }

        //R1 Save Button
        private void R1SaveButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(BooksCombo.Text))
            {
                Globals.SavedBook = Globals.SelectedBook;
                Globals.SavedBookName = BooksCombo.Text;
                if (!string.IsNullOrEmpty(FunctionCombo.Text))
                {
                    Globals.SavedFunction = Globals.SelectedFunction;
                    Globals.SavedFunctionName = FunctionCombo.Text;
                    MessageBox.Show("Details Saved!", "Saved!");
                }
                else
                    MessageBox.Show("You must select a function before saving.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("You must select a book before saving.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        //R1 Clear Response Text Box
        private void R1ClearButton_Click(object sender, EventArgs e)
        {
            R1ResponseText.Clear();
        }

        //Clear the combo boxes for BooksCombo and FunctionsCombo
        private void R1RefreshButton_Click(object sender, EventArgs e)
        {
            BooksCombo.Text = "";
            FunctionCombo.Text = "";
            Globals.SavedBook = "";
            Globals.SavedBookName = "";
            Globals.SavedFunction = "";
            Globals.SavedFunctionName = "";
            R1GoButton.Show();
            GetIdPanel.Hide();
            GetIdText.Text = "";
            DeleteIdPanel.Hide();
            DeleteIdText.Text = "";
            R1PostPanel.Hide();
            R1PostButton.Hide();
            R1PostText.Text = "";
            R1PutPanel.Hide();
            R1PutButton.Hide();
            R1PutText.Text = "";
            R1ResponsePanel.Hide();
            R1ClearButton.Hide();
            R1ResponseText.Text = "";
            ReckonOne r1 = new ReckonOne(this);
            try
            {
                r1.GetBooks();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("The application has encountered a MySQL Error. " +
                    "\r\nEnsure your MySQL Server is started and try refreshing your settings again. \r\nIf the problem persists restart the application",
                    "MySql Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nR1 Refresh/GetBooks Failed. \r\nThe following MySql exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                Console.ReadLine();
            }
            catch (JsonReaderException ex)
            {
                MessageBox.Show("Unable to retreive your list of books from the API. \r\nEnsure you login with your \"User Name\" " +
                    "and not your \"User ID\" for Reckon One. \r\n\r\nClosing Application.", "Get CashBooks Failed",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nR1 Refresh/GetBooks Failed. \r\nThe following JsonReader exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                Console.ReadLine();
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An unexpected error has occured. \r\nTry again and if the problem persists restart the application.", "Unexpected Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nR1 Refresh/GetBooks Failed. \r\nThe following exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                Console.ReadLine();
            }
        }

        //R1 Get Request Event Handler
        private void R1GoButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(R1BooksCombo) || string.IsNullOrEmpty(FunctionsCombo))
            {
                MessageBox.Show("You Must Select a Book, a GET Function and Enter the ID for the object you wish to retreive.", "Go",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);  
            }
            else
            {
                R1GoButtonEnabled = false;
                R1GoButton.Text = "GETTING";
                R1GoButton.BackColor = Color.FromArgb(106, 106, 106);
                ReckonOne r1 = new ReckonOne(this);
                try
                {
                    r1.R1GetAPIRequest("");
                }
                catch (Exception ex)
                {
                    R1GoButtonEnabled = true;
                    R1GoButtonText = "GO";
                    R1GoButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                        "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nR1 GetApiRequest Failed. \r\nThe following exception occured: \r\n{ ex.Message } \r\n\r\n{ ex.StackTrace }");
                    Console.ReadLine();
                }
            }
        }

        //R1 Get Request By ID Event Handler
        private void GetIdButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(R1BooksCombo) || string.IsNullOrEmpty(FunctionsCombo) || string.IsNullOrEmpty(R1GetId))
            {
                MessageBox.Show("You Must Select a Book, a GET Function and Enter the ID for the object you wish to retreive.", "Get",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                R1GoButtonEnabled = false;
                GetIdButton.Text = "GETTING";
                GetIdButton.BackColor = Color.FromArgb(106, 106, 106);
                ReckonOne r1 = new ReckonOne(this);
                try
                {
                    r1.R1GetByIdRequest();
                }
                catch (Exception ex)
                {
                    R1GoButtonEnabled = true;
                    R1GetIdButtonText = "GET";
                    R1GetIdButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                        "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nR1 GetByIDApiRequest Failed. \r\nThe following exception occured:\r\n{ ex.Message }\r\n\r\n{ ex.StackTrace }");
                    Console.ReadLine();
                }
            }

        }

        //R1 Delete Request Event Handler
        private void DeleteIdButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(R1BooksCombo) || string.IsNullOrEmpty(FunctionsCombo) || string.IsNullOrEmpty(R1DeleteId))
            {
                MessageBox.Show("You must enter the ID of the item you want to delete.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                R1DeleteIdButtonEnabled = false;
                DeleteIdButton.Text = "DELETING";
                DeleteIdButton.BackColor = Color.FromArgb(106, 106, 106);
                ReckonOne r1 = new ReckonOne(this);
                try
                {
                    r1.R1DeleteAPIRequest();
                }
                catch (Exception ex)
                {
                    R1DeleteIdButtonEnabled = true;
                    R1DeleteIdButtonText = "DELETE";
                    R1DeleteIdButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                        "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nR1 DeleteApiRequest Failed. \r\nThe following exception occured:\r\n{ ex.Message }\r\n\r\n{ ex.StackTrace }");
                    Console.ReadLine();
                }
            }
        }

        //R1 Post Request Event Handler
        private void R1PostButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(R1BooksCombo) || string.IsNullOrEmpty(FunctionsCombo))
            {
                MessageBox.Show("You Must Select a Book and a POST Function.", "Post", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                R1PostButtonEnabled = false;
                R1PostButton.Text = "POSTING";
                R1PostButton.BackColor = Color.FromArgb(106, 106, 106);
                ReckonOne r1 = new ReckonOne(this);
                try
                {
                    r1.R1PostAPIRequest();
                }
                catch (Exception ex)
                {
                    R1PostButtonEnabled = true;
                    R1PostButtonText = "POST";
                    R1PostButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                        "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nR1 PostApiRequest Failed. \r\nThe following exception occured:\r\n { ex.Message }\r\n\r\n { ex.StackTrace }");
                    Console.ReadLine();
                }
            }
        }

        //R1 put Request Event Handler
        private void R1PutButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(R1BooksCombo) || string.IsNullOrEmpty(FunctionsCombo))
            {
                MessageBox.Show("You Must Select a Book and a PUT Function.", "Put", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                R1PutButtonEnabled = false;
                R1PutButton.Text = "PUTTING";
                R1PutButton.BackColor = Color.FromArgb(106, 106, 106);
                ReckonOne r1 = new ReckonOne(this);
                try
                {
                    r1.R1PutAPIRequest();
                }
                catch (Exception ex)
                {
                    R1PutButtonEnabled = true;
                    R1PutButtonText = "PUT";
                    R1PutButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                        "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine($"\r\nR1 PutApiRequest Failed. \r\nThe following exception occured:\r\n { ex.Message }\r\n\r\n { ex.StackTrace }");
                    Console.ReadLine();
                }
            }
        }

        private void R1ResponseText_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(R1Resp))
            {
                R1ResponsePanel.Hide();
                R1ClearButton.Hide();
            }
            else
            {
                //Reposition Response Box Depending on Request Type
                if(Globals.SelectedFunctionType == "GET")
                {
                    R1ResponsePanel.Location = new Point(381, 196);
                    R1ClearButton.Location = new Point(586, 616);
                }
                if (Globals.SelectedFunctionType == "DELETE")
                {
                    R1ResponsePanel.Location = new Point(381, 196);
                    R1ClearButton.Location = new Point(586, 616);
                }
                if (Globals.SelectedFunctionType == "POST")
                {
                    R1ResponsePanel.Location = new Point(676, 196);
                    R1ClearButton.Location = new Point(881, 616);
                }
                if (Globals.SelectedFunctionType == "PUT")
                {
                    R1ResponsePanel.Location = new Point(972, 196);
                    R1ClearButton.Location = new Point(1177, 616);
                }
                R1GoButtonEnabled = true;
                R1GoButton.Text = "GO";
                R1GoButton.BackColor = Color.FromArgb(0, 189, 157);
                R1GetIdButtonEnabled = true;
                GetIdButton.Text = "GET";
                GetIdButton.BackColor = Color.FromArgb(0, 189, 157);
                R1GetId = "";
                R1DeleteIdButtonEnabled = true;
                DeleteIdButton.Text = "DELETE";
                DeleteIdButton.BackColor = Color.FromArgb(0, 189, 157);
                R1DeleteId = "";
                R1PostButtonEnabled = true;
                R1PostButton.Text = "POST";
                R1PostButton.BackColor = Color.FromArgb(0, 189, 157);
                R1PutButtonEnabled = true;
                R1PutButton.Text = "PUT";
                R1PutButton.BackColor = Color.FromArgb(0, 189, 157);
                R1ResponsePanel.Show();
                R1ClearButton.Show();
            }
        }

        private void RahPostButton_Click(object sender, EventArgs e)
        {
            ClearRahRespText();
            RahPostButtonEnabled = false;
            RahPostButtonText = "POSTING";
            RahPostButtonColor = Color.FromArgb(106, 106, 106);
            ReckonAccountsHosted rah = new ReckonAccountsHosted(this);
            try
            {
                Thread t = new Thread(rah.RahApiRequest);
                t.IsBackground = true;
                t.Start();
            }
            catch (Exception ex)
            {
                RahPostButtonEnabled = true;
                RahPostButtonText = "POST";
                RahPostButtonColor = Color.FromArgb(0, 189, 157);
                MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                   "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nRAH PostApiRequest Failed. \r\nThe following exception occured:\r\n { ex.Message }\r\n\r\n { ex.StackTrace }");
                Console.ReadLine();
            }
        }

        private void RahGetIdButton_Click(object sender, EventArgs e)
        {
            ClearRahRespText();
            RahGetIdButtonEnabled = false;
            RahGetIdButtonText = "GETTING";
            RahGetIdButtonColor = Color.FromArgb(106, 106, 106);
            ReckonAccountsHosted rah = new ReckonAccountsHosted(this);
            try
            {
                Thread t = new Thread(rah.RahGetGuidRequest);
                t.IsBackground = true;
                t.Start();
            }
            catch (Exception ex)
            {
                RahGetIdButtonEnabled = true;
                RahGetIdButtonText = "GET";
                RahGetIdButtonColor = Color.FromArgb(0, 189, 157);
                MessageBox.Show("An unexpected error has occured. \r\nTry again, if the problem persists restart the application.",
                   "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine($"\r\nRAH GetApiRequest Failed. \r\nThe following exception occured:\r\n { ex.Message }\r\n\r\n { ex.StackTrace }");
                Console.ReadLine();
            }
        }

        private void RahRespText_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(RahResp))
            {
                RahRespPanel.Hide();
                RahClearButton.Hide();
            }
            else
            {
                RahPostButtonEnabled = true;
                RahPostButtonText = "POST";
                RahPostButtonColor = Color.FromArgb(0, 189, 157);
                RahRespPanel.Show();
                RahClearButton.Show();
            }
        }

        private void RahRqIdText_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(RahGuid))
            {
                RahRqIdPanel.Hide();
                RahGetIdButton.Hide();
            }
            else
            {
                RahGetIdButtonEnabled = true;
                RahGetIdButtonText = "GET";
                RahGetIdButtonColor = Color.FromArgb(0, 189, 157);
                RahRqIdPanel.Show();
                RahGetIdButton.Show();
            }
        }

        private void RahFunctionsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReckonAccountsHosted rah = new ReckonAccountsHosted(this);
            rah.RAHFunctions();
            RahBodyPanel.Show();
            RahPostButton.Show();
        }

        private void RahClearButton_Click(object sender, EventArgs e)
        {
            ClearRahRespText();
            RahRespPanel.Hide();
            RahClearButton.Hide();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
 

    }
}
