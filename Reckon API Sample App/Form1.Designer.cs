﻿namespace Reckon_API_Sample_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.HomePanel = new System.Windows.Forms.Panel();
            this.R1AppSelectButton = new System.Windows.Forms.Button();
            this.RahAppSelectButton = new System.Windows.Forms.Button();
            this.WelcomeLabel = new System.Windows.Forms.Label();
            this.RahConnPanel = new System.Windows.Forms.Panel();
            this.RahConnHome = new System.Windows.Forms.PictureBox();
            this.RahConnButton = new System.Windows.Forms.Button();
            this.RahConnLabel = new System.Windows.Forms.Label();
            this.R1ConnPanel = new System.Windows.Forms.Panel();
            this.R1ConnHome = new System.Windows.Forms.PictureBox();
            this.R1ConnButton = new System.Windows.Forms.Button();
            this.R1ConnLabel = new System.Windows.Forms.Label();
            this.RahAppPanel = new System.Windows.Forms.Panel();
            this.RahAppHome = new System.Windows.Forms.PictureBox();
            this.RahClearButton = new System.Windows.Forms.Button();
            this.RahPostButton = new System.Windows.Forms.Button();
            this.RahGetIdButton = new System.Windows.Forms.Button();
            this.RahRqIdPanel = new System.Windows.Forms.Panel();
            this.RahRqIdLabel = new System.Windows.Forms.Label();
            this.RahRqIdLabelSmall = new System.Windows.Forms.Label();
            this.RahRqIdText = new System.Windows.Forms.TextBox();
            this.RahRespPanel = new System.Windows.Forms.Panel();
            this.RahRespText = new System.Windows.Forms.TextBox();
            this.RahRespLabel = new System.Windows.Forms.Label();
            this.RahBodyPanel = new System.Windows.Forms.Panel();
            this.RahBodyText = new System.Windows.Forms.TextBox();
            this.RahBodyLabel = new System.Windows.Forms.Label();
            this.RahFileDtlsPanel = new System.Windows.Forms.Panel();
            this.RahFunctionsLabel = new System.Windows.Forms.Label();
            this.RahFunctionsCombo = new System.Windows.Forms.ComboBox();
            this.VersionCombo = new System.Windows.Forms.ComboBox();
            this.FilePwdText = new System.Windows.Forms.TextBox();
            this.FileUserText = new System.Windows.Forms.TextBox();
            this.FilePathText = new System.Windows.Forms.TextBox();
            this.FileSaveButton = new System.Windows.Forms.Button();
            this.FilePwdLabel = new System.Windows.Forms.Label();
            this.VersionLabel = new System.Windows.Forms.Label();
            this.FileUserLabel = new System.Windows.Forms.Label();
            this.FilePathLabel = new System.Windows.Forms.Label();
            this.RahAppLabel = new System.Windows.Forms.Label();
            this.RahApHbPanel = new System.Windows.Forms.Panel();
            this.RahApHbStatus = new System.Windows.Forms.Label();
            this.RahApHbLabel = new System.Windows.Forms.Label();
            this.RahApAuthPanel = new System.Windows.Forms.Panel();
            this.RahApAuthLabel = new System.Windows.Forms.Label();
            this.RahApRvkPanel = new System.Windows.Forms.Panel();
            this.RahApRvkLabel = new System.Windows.Forms.Label();
            this.RahApRfshPanel = new System.Windows.Forms.Panel();
            this.RahApRfshLabel = new System.Windows.Forms.Label();
            this.RahApConnPanel = new System.Windows.Forms.Panel();
            this.RahApConnLabel = new System.Windows.Forms.Label();
            this.RahApHeaderPanel = new System.Windows.Forms.Panel();
            this.RahApHeaderLabel = new System.Windows.Forms.Label();
            this.R1AppPanel = new System.Windows.Forms.Panel();
            this.R1AppHome = new System.Windows.Forms.PictureBox();
            this.R1ClearButton = new System.Windows.Forms.Button();
            this.R1PutButton = new System.Windows.Forms.Button();
            this.R1PostButton = new System.Windows.Forms.Button();
            this.R1PostPanel = new System.Windows.Forms.Panel();
            this.R1PostText = new System.Windows.Forms.TextBox();
            this.R1PostLabel = new System.Windows.Forms.Label();
            this.R1PutPanel = new System.Windows.Forms.Panel();
            this.R1PutText = new System.Windows.Forms.TextBox();
            this.R1PutLabel = new System.Windows.Forms.Label();
            this.R1ResponsePanel = new System.Windows.Forms.Panel();
            this.R1ResponseText = new System.Windows.Forms.TextBox();
            this.R1ResponseLabel = new System.Windows.Forms.Label();
            this.DeleteIdPanel = new System.Windows.Forms.Panel();
            this.DeleteIdButton = new System.Windows.Forms.Button();
            this.DeleteIdText = new System.Windows.Forms.TextBox();
            this.DeleteIdTextLabel = new System.Windows.Forms.Label();
            this.DeleteIdLabel = new System.Windows.Forms.Label();
            this.GetIdPanel = new System.Windows.Forms.Panel();
            this.GetIdButton = new System.Windows.Forms.Button();
            this.GetIdText = new System.Windows.Forms.TextBox();
            this.GetIdTextLabel = new System.Windows.Forms.Label();
            this.GetIdLabel = new System.Windows.Forms.Label();
            this.R1RefreshButton = new System.Windows.Forms.Button();
            this.R1GoButton = new System.Windows.Forms.Button();
            this.R1SaveButton = new System.Windows.Forms.Button();
            this.FunctionCombo = new System.Windows.Forms.ComboBox();
            this.BooksCombo = new System.Windows.Forms.ComboBox();
            this.FunctionLabel = new System.Windows.Forms.Label();
            this.BookLabel = new System.Windows.Forms.Label();
            this.R1AppLabel = new System.Windows.Forms.Label();
            this.R1ApHbPanel = new System.Windows.Forms.Panel();
            this.R1ApHbStatus = new System.Windows.Forms.Label();
            this.R1ApHbLabel = new System.Windows.Forms.Label();
            this.R1ApAuthPanel = new System.Windows.Forms.Panel();
            this.R1ApAuthLabel = new System.Windows.Forms.Label();
            this.R1ApRvkPanel = new System.Windows.Forms.Panel();
            this.R1ApRvkLabel = new System.Windows.Forms.Label();
            this.R1ApRfshPanel = new System.Windows.Forms.Panel();
            this.R1ApRfshLabel = new System.Windows.Forms.Label();
            this.R1ApConnPanel = new System.Windows.Forms.Panel();
            this.R1ApConnLabel = new System.Windows.Forms.Label();
            this.R1ApHeaderPanel = new System.Windows.Forms.Panel();
            this.R1ApHeaderLabel = new System.Windows.Forms.Label();
            this.HomePanel.SuspendLayout();
            this.RahConnPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RahConnHome)).BeginInit();
            this.R1ConnPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1ConnHome)).BeginInit();
            this.RahAppPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RahAppHome)).BeginInit();
            this.RahRqIdPanel.SuspendLayout();
            this.RahRespPanel.SuspendLayout();
            this.RahBodyPanel.SuspendLayout();
            this.RahFileDtlsPanel.SuspendLayout();
            this.RahApHbPanel.SuspendLayout();
            this.RahApAuthPanel.SuspendLayout();
            this.RahApRvkPanel.SuspendLayout();
            this.RahApRfshPanel.SuspendLayout();
            this.RahApConnPanel.SuspendLayout();
            this.RahApHeaderPanel.SuspendLayout();
            this.R1AppPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1AppHome)).BeginInit();
            this.R1PostPanel.SuspendLayout();
            this.R1PutPanel.SuspendLayout();
            this.R1ResponsePanel.SuspendLayout();
            this.DeleteIdPanel.SuspendLayout();
            this.GetIdPanel.SuspendLayout();
            this.R1ApHbPanel.SuspendLayout();
            this.R1ApAuthPanel.SuspendLayout();
            this.R1ApRvkPanel.SuspendLayout();
            this.R1ApRfshPanel.SuspendLayout();
            this.R1ApConnPanel.SuspendLayout();
            this.R1ApHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // HomePanel
            // 
            this.HomePanel.BackColor = System.Drawing.Color.White;
            this.HomePanel.Controls.Add(this.R1AppSelectButton);
            this.HomePanel.Controls.Add(this.RahAppSelectButton);
            this.HomePanel.Controls.Add(this.WelcomeLabel);
            this.HomePanel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomePanel.Location = new System.Drawing.Point(324, 1);
            this.HomePanel.Name = "HomePanel";
            this.HomePanel.Size = new System.Drawing.Size(648, 408);
            this.HomePanel.TabIndex = 0;
            // 
            // R1AppSelectButton
            // 
            this.R1AppSelectButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1AppSelectButton.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1AppSelectButton.ForeColor = System.Drawing.Color.White;
            this.R1AppSelectButton.Location = new System.Drawing.Point(332, 255);
            this.R1AppSelectButton.Name = "R1AppSelectButton";
            this.R1AppSelectButton.Size = new System.Drawing.Size(300, 150);
            this.R1AppSelectButton.TabIndex = 2;
            this.R1AppSelectButton.Text = "Reckon One";
            this.R1AppSelectButton.UseVisualStyleBackColor = false;
            this.R1AppSelectButton.Click += new System.EventHandler(this.R1AppSelectButton_Click);
            // 
            // RahAppSelectButton
            // 
            this.RahAppSelectButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahAppSelectButton.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahAppSelectButton.ForeColor = System.Drawing.Color.White;
            this.RahAppSelectButton.Location = new System.Drawing.Point(8, 255);
            this.RahAppSelectButton.Name = "RahAppSelectButton";
            this.RahAppSelectButton.Size = new System.Drawing.Size(300, 150);
            this.RahAppSelectButton.TabIndex = 1;
            this.RahAppSelectButton.Text = "RA Hosted";
            this.RahAppSelectButton.UseVisualStyleBackColor = false;
            this.RahAppSelectButton.Click += new System.EventHandler(this.RahAppSelectButton_Click);
            // 
            // WelcomeLabel
            // 
            this.WelcomeLabel.AutoSize = true;
            this.WelcomeLabel.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WelcomeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.WelcomeLabel.Location = new System.Drawing.Point(111, 110);
            this.WelcomeLabel.Name = "WelcomeLabel";
            this.WelcomeLabel.Size = new System.Drawing.Size(455, 87);
            this.WelcomeLabel.TabIndex = 0;
            this.WelcomeLabel.Text = "Welcome to Reckon API Sample App!\r\n\r\nPlease choose API Product:";
            this.WelcomeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RahConnPanel
            // 
            this.RahConnPanel.Controls.Add(this.RahConnHome);
            this.RahConnPanel.Controls.Add(this.RahConnButton);
            this.RahConnPanel.Controls.Add(this.RahConnLabel);
            this.RahConnPanel.Location = new System.Drawing.Point(3, 4);
            this.RahConnPanel.Name = "RahConnPanel";
            this.RahConnPanel.Size = new System.Drawing.Size(345, 246);
            this.RahConnPanel.TabIndex = 1;
            // 
            // RahConnHome
            // 
            this.RahConnHome.BackColor = System.Drawing.Color.Transparent;
            this.RahConnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RahConnHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahConnHome.ErrorImage = ((System.Drawing.Image)(resources.GetObject("RahConnHome.ErrorImage")));
            this.RahConnHome.Image = ((System.Drawing.Image)(resources.GetObject("RahConnHome.Image")));
            this.RahConnHome.Location = new System.Drawing.Point(3, 3);
            this.RahConnHome.Name = "RahConnHome";
            this.RahConnHome.Size = new System.Drawing.Size(24, 21);
            this.RahConnHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RahConnHome.TabIndex = 4;
            this.RahConnHome.TabStop = false;
            this.RahConnHome.Click += new System.EventHandler(this.RahConnHome_Click);
            // 
            // RahConnButton
            // 
            this.RahConnButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahConnButton.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahConnButton.ForeColor = System.Drawing.Color.White;
            this.RahConnButton.Location = new System.Drawing.Point(33, 85);
            this.RahConnButton.Name = "RahConnButton";
            this.RahConnButton.Size = new System.Drawing.Size(300, 150);
            this.RahConnButton.TabIndex = 3;
            this.RahConnButton.Text = "Connect";
            this.RahConnButton.UseVisualStyleBackColor = false;
            this.RahConnButton.Click += new System.EventHandler(this.RahConnButton_Click);
            // 
            // RahConnLabel
            // 
            this.RahConnLabel.AutoSize = true;
            this.RahConnLabel.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahConnLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahConnLabel.Location = new System.Drawing.Point(28, 32);
            this.RahConnLabel.Name = "RahConnLabel";
            this.RahConnLabel.Size = new System.Drawing.Size(309, 29);
            this.RahConnLabel.TabIndex = 1;
            this.RahConnLabel.Text = "Reckon Accounts Hosted";
            this.RahConnLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // R1ConnPanel
            // 
            this.R1ConnPanel.Controls.Add(this.R1ConnHome);
            this.R1ConnPanel.Controls.Add(this.R1ConnButton);
            this.R1ConnPanel.Controls.Add(this.R1ConnLabel);
            this.R1ConnPanel.Location = new System.Drawing.Point(3, 4);
            this.R1ConnPanel.Name = "R1ConnPanel";
            this.R1ConnPanel.Size = new System.Drawing.Size(336, 246);
            this.R1ConnPanel.TabIndex = 2;
            // 
            // R1ConnHome
            // 
            this.R1ConnHome.BackColor = System.Drawing.Color.Transparent;
            this.R1ConnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1ConnHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ConnHome.ErrorImage = ((System.Drawing.Image)(resources.GetObject("R1ConnHome.ErrorImage")));
            this.R1ConnHome.Image = ((System.Drawing.Image)(resources.GetObject("R1ConnHome.Image")));
            this.R1ConnHome.Location = new System.Drawing.Point(3, 3);
            this.R1ConnHome.Name = "R1ConnHome";
            this.R1ConnHome.Size = new System.Drawing.Size(24, 21);
            this.R1ConnHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R1ConnHome.TabIndex = 5;
            this.R1ConnHome.TabStop = false;
            this.R1ConnHome.Click += new System.EventHandler(this.R1ConnHome_Click);
            // 
            // R1ConnButton
            // 
            this.R1ConnButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ConnButton.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ConnButton.ForeColor = System.Drawing.Color.White;
            this.R1ConnButton.Location = new System.Drawing.Point(23, 85);
            this.R1ConnButton.Name = "R1ConnButton";
            this.R1ConnButton.Size = new System.Drawing.Size(300, 150);
            this.R1ConnButton.TabIndex = 3;
            this.R1ConnButton.Text = "Connect";
            this.R1ConnButton.UseVisualStyleBackColor = false;
            this.R1ConnButton.Click += new System.EventHandler(this.R1ConnButton_Click);
            // 
            // R1ConnLabel
            // 
            this.R1ConnLabel.AutoSize = true;
            this.R1ConnLabel.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ConnLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1ConnLabel.Location = new System.Drawing.Point(18, 32);
            this.R1ConnLabel.Name = "R1ConnLabel";
            this.R1ConnLabel.Size = new System.Drawing.Size(156, 29);
            this.R1ConnLabel.TabIndex = 1;
            this.R1ConnLabel.Text = "Reckon One";
            this.R1ConnLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RahAppPanel
            // 
            this.RahAppPanel.Controls.Add(this.RahAppHome);
            this.RahAppPanel.Controls.Add(this.RahClearButton);
            this.RahAppPanel.Controls.Add(this.RahPostButton);
            this.RahAppPanel.Controls.Add(this.RahGetIdButton);
            this.RahAppPanel.Controls.Add(this.RahRqIdPanel);
            this.RahAppPanel.Controls.Add(this.RahRespPanel);
            this.RahAppPanel.Controls.Add(this.RahBodyPanel);
            this.RahAppPanel.Controls.Add(this.RahFileDtlsPanel);
            this.RahAppPanel.Controls.Add(this.RahAppLabel);
            this.RahAppPanel.Controls.Add(this.RahApHbPanel);
            this.RahAppPanel.Controls.Add(this.RahApAuthPanel);
            this.RahAppPanel.Controls.Add(this.RahApRvkPanel);
            this.RahAppPanel.Controls.Add(this.RahApRfshPanel);
            this.RahAppPanel.Controls.Add(this.RahApConnPanel);
            this.RahAppPanel.Controls.Add(this.RahApHeaderPanel);
            this.RahAppPanel.Location = new System.Drawing.Point(2, 4);
            this.RahAppPanel.Name = "RahAppPanel";
            this.RahAppPanel.Size = new System.Drawing.Size(1264, 665);
            this.RahAppPanel.TabIndex = 3;
            // 
            // RahAppHome
            // 
            this.RahAppHome.BackColor = System.Drawing.Color.Transparent;
            this.RahAppHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RahAppHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahAppHome.ErrorImage = ((System.Drawing.Image)(resources.GetObject("RahAppHome.ErrorImage")));
            this.RahAppHome.Image = ((System.Drawing.Image)(resources.GetObject("RahAppHome.Image")));
            this.RahAppHome.Location = new System.Drawing.Point(3, 3);
            this.RahAppHome.Name = "RahAppHome";
            this.RahAppHome.Size = new System.Drawing.Size(24, 21);
            this.RahAppHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RahAppHome.TabIndex = 17;
            this.RahAppHome.TabStop = false;
            this.RahAppHome.Click += new System.EventHandler(this.RahAppHome_Click);
            // 
            // RahClearButton
            // 
            this.RahClearButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahClearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RahClearButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahClearButton.FlatAppearance.BorderSize = 0;
            this.RahClearButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahClearButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RahClearButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahClearButton.ForeColor = System.Drawing.Color.White;
            this.RahClearButton.Location = new System.Drawing.Point(860, 622);
            this.RahClearButton.Name = "RahClearButton";
            this.RahClearButton.Size = new System.Drawing.Size(75, 24);
            this.RahClearButton.TabIndex = 16;
            this.RahClearButton.Text = "CLEAR";
            this.RahClearButton.UseVisualStyleBackColor = false;
            this.RahClearButton.Click += new System.EventHandler(this.RahClearButton_Click);
            // 
            // RahPostButton
            // 
            this.RahPostButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahPostButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RahPostButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahPostButton.FlatAppearance.BorderSize = 0;
            this.RahPostButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RahPostButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahPostButton.ForeColor = System.Drawing.Color.White;
            this.RahPostButton.Location = new System.Drawing.Point(443, 622);
            this.RahPostButton.Name = "RahPostButton";
            this.RahPostButton.Size = new System.Drawing.Size(75, 24);
            this.RahPostButton.TabIndex = 15;
            this.RahPostButton.Text = "POST";
            this.RahPostButton.UseVisualStyleBackColor = false;
            this.RahPostButton.Click += new System.EventHandler(this.RahPostButton_Click);
            // 
            // RahGetIdButton
            // 
            this.RahGetIdButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahGetIdButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RahGetIdButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahGetIdButton.FlatAppearance.BorderSize = 0;
            this.RahGetIdButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahGetIdButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahGetIdButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RahGetIdButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahGetIdButton.ForeColor = System.Drawing.Color.White;
            this.RahGetIdButton.Location = new System.Drawing.Point(1177, 364);
            this.RahGetIdButton.Name = "RahGetIdButton";
            this.RahGetIdButton.Size = new System.Drawing.Size(75, 24);
            this.RahGetIdButton.TabIndex = 14;
            this.RahGetIdButton.Text = "GET";
            this.RahGetIdButton.UseVisualStyleBackColor = false;
            this.RahGetIdButton.Click += new System.EventHandler(this.RahGetIdButton_Click);
            // 
            // RahRqIdPanel
            // 
            this.RahRqIdPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahRqIdPanel.Controls.Add(this.RahRqIdLabel);
            this.RahRqIdPanel.Controls.Add(this.RahRqIdLabelSmall);
            this.RahRqIdPanel.Controls.Add(this.RahRqIdText);
            this.RahRqIdPanel.Location = new System.Drawing.Point(941, 228);
            this.RahRqIdPanel.Name = "RahRqIdPanel";
            this.RahRqIdPanel.Size = new System.Drawing.Size(311, 132);
            this.RahRqIdPanel.TabIndex = 6;
            // 
            // RahRqIdLabel
            // 
            this.RahRqIdLabel.AutoSize = true;
            this.RahRqIdLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahRqIdLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahRqIdLabel.Location = new System.Drawing.Point(10, 10);
            this.RahRqIdLabel.Name = "RahRqIdLabel";
            this.RahRqIdLabel.Size = new System.Drawing.Size(119, 18);
            this.RahRqIdLabel.TabIndex = 7;
            this.RahRqIdLabel.Text = "REQUEST ID";
            // 
            // RahRqIdLabelSmall
            // 
            this.RahRqIdLabelSmall.AutoSize = true;
            this.RahRqIdLabelSmall.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahRqIdLabelSmall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahRqIdLabelSmall.Location = new System.Drawing.Point(10, 44);
            this.RahRqIdLabelSmall.Name = "RahRqIdLabelSmall";
            this.RahRqIdLabelSmall.Size = new System.Drawing.Size(80, 16);
            this.RahRqIdLabelSmall.TabIndex = 1;
            this.RahRqIdLabelSmall.Text = "Request ID";
            // 
            // RahRqIdText
            // 
            this.RahRqIdText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahRqIdText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahRqIdText.Location = new System.Drawing.Point(13, 73);
            this.RahRqIdText.Name = "RahRqIdText";
            this.RahRqIdText.Size = new System.Drawing.Size(287, 23);
            this.RahRqIdText.TabIndex = 0;
            this.RahRqIdText.TextChanged += new System.EventHandler(this.RahRqIdText_TextChanged);
            // 
            // RahRespPanel
            // 
            this.RahRespPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahRespPanel.Controls.Add(this.RahRespText);
            this.RahRespPanel.Controls.Add(this.RahRespLabel);
            this.RahRespPanel.Location = new System.Drawing.Point(524, 228);
            this.RahRespPanel.Name = "RahRespPanel";
            this.RahRespPanel.Size = new System.Drawing.Size(411, 388);
            this.RahRespPanel.TabIndex = 6;
            // 
            // RahRespText
            // 
            this.RahRespText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahRespText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahRespText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahRespText.Location = new System.Drawing.Point(3, 31);
            this.RahRespText.MaxLength = 999999999;
            this.RahRespText.Multiline = true;
            this.RahRespText.Name = "RahRespText";
            this.RahRespText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RahRespText.Size = new System.Drawing.Size(403, 352);
            this.RahRespText.TabIndex = 16;
            this.RahRespText.TextChanged += new System.EventHandler(this.RahRespText_TextChanged);
            // 
            // RahRespLabel
            // 
            this.RahRespLabel.AutoSize = true;
            this.RahRespLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahRespLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahRespLabel.Location = new System.Drawing.Point(10, 10);
            this.RahRespLabel.Name = "RahRespLabel";
            this.RahRespLabel.Size = new System.Drawing.Size(104, 18);
            this.RahRespLabel.TabIndex = 9;
            this.RahRespLabel.Text = "RESPONSE";
            // 
            // RahBodyPanel
            // 
            this.RahBodyPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahBodyPanel.Controls.Add(this.RahBodyText);
            this.RahBodyPanel.Controls.Add(this.RahBodyLabel);
            this.RahBodyPanel.Location = new System.Drawing.Point(14, 228);
            this.RahBodyPanel.Name = "RahBodyPanel";
            this.RahBodyPanel.Size = new System.Drawing.Size(504, 388);
            this.RahBodyPanel.TabIndex = 5;
            // 
            // RahBodyText
            // 
            this.RahBodyText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahBodyText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahBodyText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahBodyText.ImeMode = System.Windows.Forms.ImeMode.On;
            this.RahBodyText.Location = new System.Drawing.Point(3, 31);
            this.RahBodyText.MaxLength = 999999999;
            this.RahBodyText.Multiline = true;
            this.RahBodyText.Name = "RahBodyText";
            this.RahBodyText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RahBodyText.Size = new System.Drawing.Size(496, 352);
            this.RahBodyText.TabIndex = 15;
            this.RahBodyText.WordWrap = false;
            // 
            // RahBodyLabel
            // 
            this.RahBodyLabel.AutoSize = true;
            this.RahBodyLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahBodyLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahBodyLabel.Location = new System.Drawing.Point(11, 10);
            this.RahBodyLabel.Name = "RahBodyLabel";
            this.RahBodyLabel.Size = new System.Drawing.Size(59, 18);
            this.RahBodyLabel.TabIndex = 8;
            this.RahBodyLabel.Text = "BODY";
            // 
            // RahFileDtlsPanel
            // 
            this.RahFileDtlsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahFileDtlsPanel.Controls.Add(this.RahFunctionsLabel);
            this.RahFileDtlsPanel.Controls.Add(this.RahFunctionsCombo);
            this.RahFileDtlsPanel.Controls.Add(this.VersionCombo);
            this.RahFileDtlsPanel.Controls.Add(this.FilePwdText);
            this.RahFileDtlsPanel.Controls.Add(this.FileUserText);
            this.RahFileDtlsPanel.Controls.Add(this.FilePathText);
            this.RahFileDtlsPanel.Controls.Add(this.FileSaveButton);
            this.RahFileDtlsPanel.Controls.Add(this.FilePwdLabel);
            this.RahFileDtlsPanel.Controls.Add(this.VersionLabel);
            this.RahFileDtlsPanel.Controls.Add(this.FileUserLabel);
            this.RahFileDtlsPanel.Controls.Add(this.FilePathLabel);
            this.RahFileDtlsPanel.Location = new System.Drawing.Point(14, 85);
            this.RahFileDtlsPanel.Name = "RahFileDtlsPanel";
            this.RahFileDtlsPanel.Size = new System.Drawing.Size(798, 128);
            this.RahFileDtlsPanel.TabIndex = 4;
            // 
            // RahFunctionsLabel
            // 
            this.RahFunctionsLabel.AutoSize = true;
            this.RahFunctionsLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahFunctionsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahFunctionsLabel.Location = new System.Drawing.Point(379, 83);
            this.RahFunctionsLabel.Name = "RahFunctionsLabel";
            this.RahFunctionsLabel.Size = new System.Drawing.Size(72, 16);
            this.RahFunctionsLabel.TabIndex = 15;
            this.RahFunctionsLabel.Text = "Functions";
            // 
            // RahFunctionsCombo
            // 
            this.RahFunctionsCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.RahFunctionsCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.RahFunctionsCombo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahFunctionsCombo.ItemHeight = 16;
            this.RahFunctionsCombo.Items.AddRange(new object[] {
            "POST Host(Query)",
            "POST Company(Query)",
            "POST CompanyActivity(Query)",
            "POST Preferences(Query)",
            "POST Account(Query)",
            "POST Customer(Query)",
            "POST Employee(Query)",
            "POST OtherName(Query)",
            "POST Vendor(Query)",
            "POST Entity(Query)",
            "POST StandardTerms(Query)",
            "POST DateDrivenTerms(Query)",
            "POST Terms(Query)",
            "POST Class(Query)",
            "POST SalesRep(Query)",
            "POST ItemService(Query)",
            "POST ItemNonInventory(Query)",
            "POST ItemOtherCharge(Query)",
            "POST ItemInventory(Query)",
            "POST ItemInventoryAssembly(Query)",
            "POST ItemSubtotal(Query)",
            "POST ItemDiscount(Query)",
            "POST ItemPayment(Query)",
            "POST ItemSalesTax(Query)",
            "POST ItemGroup(Query)",
            "POST Item(Query)",
            "POST Template(Query)",
            "POST Invoice(Query)",
            "POST Estimate(Query)",
            "POST SalesOrder(Query)",
            "POST SalesReceipt(Query)",
            "POST CreditMemo(Query)",
            "POST ReceivePayment(Query)",
            "POST ReceivePaymentToDeposit(Query)",
            "POST PurchaseOrder(Query)",
            "POST Bill(Query)",
            "POST ItemReceipt(Query)",
            "POST VendorCredit(Query)",
            "POST BillPaymentCheck(Query)",
            "POST BillPaymentCreditCard(Query)",
            "POST InventoryAdjustment(Query)",
            "POST TimeTracking(Query)",
            "POST Check(Query)",
            "POST CreditCardCharge(Query)",
            "POST CreditCardCredit(Query)",
            "POST JournalEntry(Query)",
            "POST Deposit(Query)",
            "POST PriceLevel(Query)",
            "POST Account(Add)",
            "POST Customer(Add)",
            "POST Vendor(Add)",
            "POST Class(Add)",
            "POST ItemService(Add)",
            "POST ItemNonInventory(Add)",
            "POST ItemInventory(Add)",
            "POST Invoice(Add)",
            "POST Estimate(Add)",
            "POST SalesOrder(Add)",
            "POST SalesReceipt(Add)",
            "POST CreditMemo(Add)",
            "POST ReceivePayment(Add)",
            "POST PurchaseOrder(Add)",
            "POST Bill(Add)",
            "POST ItemReceipt(Add)",
            "POST VendorCredit(Add)",
            "POST BillPaymentCheck(Add)",
            "POST Check(Add)",
            "POST JournalEntry(Add)",
            "POST Deposit(Add)"});
            this.RahFunctionsCombo.Location = new System.Drawing.Point(457, 80);
            this.RahFunctionsCombo.Name = "RahFunctionsCombo";
            this.RahFunctionsCombo.Size = new System.Drawing.Size(230, 24);
            this.RahFunctionsCombo.TabIndex = 14;
            this.RahFunctionsCombo.SelectedValueChanged += new System.EventHandler(this.RahFunctionsCombo_SelectedIndexChanged);
            // 
            // VersionCombo
            // 
            this.VersionCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.VersionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.VersionCombo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VersionCombo.FormattingEnabled = true;
            this.VersionCombo.ItemHeight = 16;
            this.VersionCombo.Location = new System.Drawing.Point(119, 80);
            this.VersionCombo.Name = "VersionCombo";
            this.VersionCombo.Size = new System.Drawing.Size(230, 24);
            this.VersionCombo.TabIndex = 13;
            // 
            // FilePwdText
            // 
            this.FilePwdText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FilePwdText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePwdText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FilePwdText.Location = new System.Drawing.Point(457, 46);
            this.FilePwdText.MaxLength = 999999999;
            this.FilePwdText.Name = "FilePwdText";
            this.FilePwdText.Size = new System.Drawing.Size(230, 23);
            this.FilePwdText.TabIndex = 12;
            // 
            // FileUserText
            // 
            this.FileUserText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FileUserText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileUserText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FileUserText.Location = new System.Drawing.Point(119, 46);
            this.FileUserText.MaxLength = 999999999;
            this.FileUserText.Name = "FileUserText";
            this.FileUserText.Size = new System.Drawing.Size(230, 23);
            this.FileUserText.TabIndex = 10;
            // 
            // FilePathText
            // 
            this.FilePathText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FilePathText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePathText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FilePathText.Location = new System.Drawing.Point(119, 11);
            this.FilePathText.MaxLength = 999999999;
            this.FilePathText.Name = "FilePathText";
            this.FilePathText.Size = new System.Drawing.Size(568, 23);
            this.FilePathText.TabIndex = 9;
            // 
            // FileSaveButton
            // 
            this.FileSaveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.FileSaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.FileSaveButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.FileSaveButton.FlatAppearance.BorderSize = 0;
            this.FileSaveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.FileSaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.FileSaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FileSaveButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileSaveButton.ForeColor = System.Drawing.Color.White;
            this.FileSaveButton.Location = new System.Drawing.Point(703, 11);
            this.FileSaveButton.Name = "FileSaveButton";
            this.FileSaveButton.Size = new System.Drawing.Size(75, 24);
            this.FileSaveButton.TabIndex = 8;
            this.FileSaveButton.Text = "Save";
            this.FileSaveButton.UseVisualStyleBackColor = false;
            this.FileSaveButton.Click += new System.EventHandler(this.FileSaveButton_Click);
            // 
            // FilePwdLabel
            // 
            this.FilePwdLabel.AutoSize = true;
            this.FilePwdLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePwdLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FilePwdLabel.Location = new System.Drawing.Point(354, 50);
            this.FilePwdLabel.Name = "FilePwdLabel";
            this.FilePwdLabel.Size = new System.Drawing.Size(97, 16);
            this.FilePwdLabel.TabIndex = 3;
            this.FilePwdLabel.Text = "File Password";
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VersionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.VersionLabel.Location = new System.Drawing.Point(2, 83);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.Size = new System.Drawing.Size(111, 16);
            this.VersionLabel.TabIndex = 2;
            this.VersionLabel.Text = "Product Version";
            // 
            // FileUserLabel
            // 
            this.FileUserLabel.AutoSize = true;
            this.FileUserLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileUserLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FileUserLabel.Location = new System.Drawing.Point(8, 50);
            this.FileUserLabel.Name = "FileUserLabel";
            this.FileUserLabel.Size = new System.Drawing.Size(105, 16);
            this.FileUserLabel.TabIndex = 1;
            this.FileUserLabel.Text = "File User Name";
            // 
            // FilePathLabel
            // 
            this.FilePathLabel.AutoSize = true;
            this.FilePathLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilePathLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FilePathLabel.Location = new System.Drawing.Point(48, 13);
            this.FilePathLabel.Name = "FilePathLabel";
            this.FilePathLabel.Size = new System.Drawing.Size(65, 16);
            this.FilePathLabel.TabIndex = 0;
            this.FilePathLabel.Text = "File Path";
            // 
            // RahAppLabel
            // 
            this.RahAppLabel.AutoSize = true;
            this.RahAppLabel.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahAppLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahAppLabel.Location = new System.Drawing.Point(9, 32);
            this.RahAppLabel.Name = "RahAppLabel";
            this.RahAppLabel.Size = new System.Drawing.Size(309, 29);
            this.RahAppLabel.TabIndex = 3;
            this.RahAppLabel.Text = "Reckon Accounts Hosted";
            // 
            // RahApHbPanel
            // 
            this.RahApHbPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahApHbPanel.Controls.Add(this.RahApHbStatus);
            this.RahApHbPanel.Controls.Add(this.RahApHbLabel);
            this.RahApHbPanel.Location = new System.Drawing.Point(1129, 140);
            this.RahApHbPanel.Name = "RahApHbPanel";
            this.RahApHbPanel.Size = new System.Drawing.Size(123, 27);
            this.RahApHbPanel.TabIndex = 1;
            // 
            // RahApHbStatus
            // 
            this.RahApHbStatus.AutoSize = true;
            this.RahApHbStatus.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApHbStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahApHbStatus.Location = new System.Drawing.Point(87, 6);
            this.RahApHbStatus.Name = "RahApHbStatus";
            this.RahApHbStatus.Size = new System.Drawing.Size(0, 14);
            this.RahApHbStatus.TabIndex = 5;
            this.RahApHbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RahApHbLabel
            // 
            this.RahApHbLabel.AutoSize = true;
            this.RahApHbLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApHbLabel.Location = new System.Drawing.Point(16, 6);
            this.RahApHbLabel.Name = "RahApHbLabel";
            this.RahApHbLabel.Size = new System.Drawing.Size(80, 14);
            this.RahApHbLabel.TabIndex = 4;
            this.RahApHbLabel.Text = "Heartbeat: ";
            this.RahApHbLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RahApAuthPanel
            // 
            this.RahApAuthPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahApAuthPanel.Controls.Add(this.RahApAuthLabel);
            this.RahApAuthPanel.Location = new System.Drawing.Point(1129, 114);
            this.RahApAuthPanel.Name = "RahApAuthPanel";
            this.RahApAuthPanel.Size = new System.Drawing.Size(123, 27);
            this.RahApAuthPanel.TabIndex = 1;
            // 
            // RahApAuthLabel
            // 
            this.RahApAuthLabel.AutoSize = true;
            this.RahApAuthLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RahApAuthLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApAuthLabel.Location = new System.Drawing.Point(24, 6);
            this.RahApAuthLabel.Name = "RahApAuthLabel";
            this.RahApAuthLabel.Size = new System.Drawing.Size(75, 14);
            this.RahApAuthLabel.TabIndex = 3;
            this.RahApAuthLabel.Text = "Auth Again";
            this.RahApAuthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RahApAuthLabel.Click += new System.EventHandler(this.RahApAuthLabel_Click);
            // 
            // RahApRvkPanel
            // 
            this.RahApRvkPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahApRvkPanel.Controls.Add(this.RahApRvkLabel);
            this.RahApRvkPanel.Location = new System.Drawing.Point(1129, 90);
            this.RahApRvkPanel.Name = "RahApRvkPanel";
            this.RahApRvkPanel.Size = new System.Drawing.Size(123, 27);
            this.RahApRvkPanel.TabIndex = 1;
            // 
            // RahApRvkLabel
            // 
            this.RahApRvkLabel.AutoSize = true;
            this.RahApRvkLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RahApRvkLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApRvkLabel.Location = new System.Drawing.Point(13, 4);
            this.RahApRvkLabel.Name = "RahApRvkLabel";
            this.RahApRvkLabel.Size = new System.Drawing.Size(99, 14);
            this.RahApRvkLabel.TabIndex = 2;
            this.RahApRvkLabel.Text = "Revoke Access";
            this.RahApRvkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RahApRvkLabel.Click += new System.EventHandler(this.RahApRvkLabel_Click);
            // 
            // RahApRfshPanel
            // 
            this.RahApRfshPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahApRfshPanel.Controls.Add(this.RahApRfshLabel);
            this.RahApRfshPanel.Location = new System.Drawing.Point(1129, 64);
            this.RahApRfshPanel.Name = "RahApRfshPanel";
            this.RahApRfshPanel.Size = new System.Drawing.Size(123, 27);
            this.RahApRfshPanel.TabIndex = 1;
            // 
            // RahApRfshLabel
            // 
            this.RahApRfshLabel.AutoSize = true;
            this.RahApRfshLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RahApRfshLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApRfshLabel.Location = new System.Drawing.Point(14, 6);
            this.RahApRfshLabel.Name = "RahApRfshLabel";
            this.RahApRfshLabel.Size = new System.Drawing.Size(96, 14);
            this.RahApRfshLabel.TabIndex = 1;
            this.RahApRfshLabel.Text = "Refresh Token";
            this.RahApRfshLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RahApRfshLabel.Click += new System.EventHandler(this.RahApRfshLabel_Click);
            // 
            // RahApConnPanel
            // 
            this.RahApConnPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahApConnPanel.Controls.Add(this.RahApConnLabel);
            this.RahApConnPanel.Location = new System.Drawing.Point(1129, 38);
            this.RahApConnPanel.Name = "RahApConnPanel";
            this.RahApConnPanel.Size = new System.Drawing.Size(123, 27);
            this.RahApConnPanel.TabIndex = 1;
            // 
            // RahApConnLabel
            // 
            this.RahApConnLabel.AutoSize = true;
            this.RahApConnLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApConnLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.RahApConnLabel.Location = new System.Drawing.Point(19, 6);
            this.RahApConnLabel.Name = "RahApConnLabel";
            this.RahApConnLabel.Size = new System.Drawing.Size(0, 14);
            this.RahApConnLabel.TabIndex = 0;
            this.RahApConnLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RahApHeaderPanel
            // 
            this.RahApHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.RahApHeaderPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RahApHeaderPanel.Controls.Add(this.RahApHeaderLabel);
            this.RahApHeaderPanel.Location = new System.Drawing.Point(1129, 12);
            this.RahApHeaderPanel.Name = "RahApHeaderPanel";
            this.RahApHeaderPanel.Size = new System.Drawing.Size(123, 27);
            this.RahApHeaderPanel.TabIndex = 0;
            // 
            // RahApHeaderLabel
            // 
            this.RahApHeaderLabel.AutoSize = true;
            this.RahApHeaderLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RahApHeaderLabel.ForeColor = System.Drawing.Color.White;
            this.RahApHeaderLabel.Location = new System.Drawing.Point(18, 5);
            this.RahApHeaderLabel.Name = "RahApHeaderLabel";
            this.RahApHeaderLabel.Size = new System.Drawing.Size(87, 16);
            this.RahApHeaderLabel.TabIndex = 0;
            this.RahApHeaderLabel.Text = "Admin Panel";
            this.RahApHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // R1AppPanel
            // 
            this.R1AppPanel.Controls.Add(this.R1AppHome);
            this.R1AppPanel.Controls.Add(this.R1ClearButton);
            this.R1AppPanel.Controls.Add(this.R1PutButton);
            this.R1AppPanel.Controls.Add(this.R1PostButton);
            this.R1AppPanel.Controls.Add(this.R1PostPanel);
            this.R1AppPanel.Controls.Add(this.R1PutPanel);
            this.R1AppPanel.Controls.Add(this.R1ResponsePanel);
            this.R1AppPanel.Controls.Add(this.DeleteIdPanel);
            this.R1AppPanel.Controls.Add(this.GetIdPanel);
            this.R1AppPanel.Controls.Add(this.R1RefreshButton);
            this.R1AppPanel.Controls.Add(this.R1GoButton);
            this.R1AppPanel.Controls.Add(this.R1SaveButton);
            this.R1AppPanel.Controls.Add(this.FunctionCombo);
            this.R1AppPanel.Controls.Add(this.BooksCombo);
            this.R1AppPanel.Controls.Add(this.FunctionLabel);
            this.R1AppPanel.Controls.Add(this.BookLabel);
            this.R1AppPanel.Controls.Add(this.R1AppLabel);
            this.R1AppPanel.Controls.Add(this.R1ApHbPanel);
            this.R1AppPanel.Controls.Add(this.R1ApAuthPanel);
            this.R1AppPanel.Controls.Add(this.R1ApRvkPanel);
            this.R1AppPanel.Controls.Add(this.R1ApRfshPanel);
            this.R1AppPanel.Controls.Add(this.R1ApConnPanel);
            this.R1AppPanel.Controls.Add(this.R1ApHeaderPanel);
            this.R1AppPanel.Location = new System.Drawing.Point(0, 2);
            this.R1AppPanel.Name = "R1AppPanel";
            this.R1AppPanel.Size = new System.Drawing.Size(1264, 664);
            this.R1AppPanel.TabIndex = 4;
            // 
            // R1AppHome
            // 
            this.R1AppHome.BackColor = System.Drawing.Color.Transparent;
            this.R1AppHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1AppHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1AppHome.ErrorImage = ((System.Drawing.Image)(resources.GetObject("R1AppHome.ErrorImage")));
            this.R1AppHome.Image = ((System.Drawing.Image)(resources.GetObject("R1AppHome.Image")));
            this.R1AppHome.Location = new System.Drawing.Point(3, 3);
            this.R1AppHome.Name = "R1AppHome";
            this.R1AppHome.Size = new System.Drawing.Size(24, 21);
            this.R1AppHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R1AppHome.TabIndex = 18;
            this.R1AppHome.TabStop = false;
            this.R1AppHome.Click += new System.EventHandler(this.R1AppHome_Click);
            // 
            // R1ClearButton
            // 
            this.R1ClearButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ClearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1ClearButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ClearButton.FlatAppearance.BorderSize = 0;
            this.R1ClearButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ClearButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1ClearButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ClearButton.ForeColor = System.Drawing.Color.White;
            this.R1ClearButton.Location = new System.Drawing.Point(1177, 616);
            this.R1ClearButton.Name = "R1ClearButton";
            this.R1ClearButton.Size = new System.Drawing.Size(75, 24);
            this.R1ClearButton.TabIndex = 20;
            this.R1ClearButton.Text = "CLEAR";
            this.R1ClearButton.UseVisualStyleBackColor = false;
            this.R1ClearButton.Click += new System.EventHandler(this.R1ClearButton_Click);
            // 
            // R1PutButton
            // 
            this.R1PutButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1PutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1PutButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1PutButton.FlatAppearance.BorderSize = 0;
            this.R1PutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1PutButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1PutButton.ForeColor = System.Drawing.Color.White;
            this.R1PutButton.Location = new System.Drawing.Point(881, 616);
            this.R1PutButton.Name = "R1PutButton";
            this.R1PutButton.Size = new System.Drawing.Size(75, 24);
            this.R1PutButton.TabIndex = 19;
            this.R1PutButton.Text = "PUT";
            this.R1PutButton.UseVisualStyleBackColor = false;
            this.R1PutButton.Click += new System.EventHandler(this.R1PutButton_Click);
            // 
            // R1PostButton
            // 
            this.R1PostButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1PostButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1PostButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1PostButton.FlatAppearance.BorderSize = 0;
            this.R1PostButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1PostButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1PostButton.ForeColor = System.Drawing.Color.White;
            this.R1PostButton.Location = new System.Drawing.Point(586, 616);
            this.R1PostButton.Name = "R1PostButton";
            this.R1PostButton.Size = new System.Drawing.Size(75, 24);
            this.R1PostButton.TabIndex = 18;
            this.R1PostButton.Text = "POST";
            this.R1PostButton.UseVisualStyleBackColor = false;
            this.R1PostButton.Click += new System.EventHandler(this.R1PostButton_Click);
            // 
            // R1PostPanel
            // 
            this.R1PostPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1PostPanel.Controls.Add(this.R1PostText);
            this.R1PostPanel.Controls.Add(this.R1PostLabel);
            this.R1PostPanel.Location = new System.Drawing.Point(381, 196);
            this.R1PostPanel.Name = "R1PostPanel";
            this.R1PostPanel.Size = new System.Drawing.Size(280, 414);
            this.R1PostPanel.TabIndex = 17;
            // 
            // R1PostText
            // 
            this.R1PostText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1PostText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1PostText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1PostText.Location = new System.Drawing.Point(3, 27);
            this.R1PostText.MaxLength = 999999999;
            this.R1PostText.Multiline = true;
            this.R1PostText.Name = "R1PostText";
            this.R1PostText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.R1PostText.Size = new System.Drawing.Size(272, 382);
            this.R1PostText.TabIndex = 14;
            this.R1PostText.WordWrap = false;
            // 
            // R1PostLabel
            // 
            this.R1PostLabel.AutoSize = true;
            this.R1PostLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1PostLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1PostLabel.Location = new System.Drawing.Point(3, 5);
            this.R1PostLabel.Name = "R1PostLabel";
            this.R1PostLabel.Size = new System.Drawing.Size(57, 18);
            this.R1PostLabel.TabIndex = 13;
            this.R1PostLabel.Text = "POST";
            // 
            // R1PutPanel
            // 
            this.R1PutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1PutPanel.Controls.Add(this.R1PutText);
            this.R1PutPanel.Controls.Add(this.R1PutLabel);
            this.R1PutPanel.Location = new System.Drawing.Point(676, 196);
            this.R1PutPanel.Name = "R1PutPanel";
            this.R1PutPanel.Size = new System.Drawing.Size(280, 414);
            this.R1PutPanel.TabIndex = 17;
            // 
            // R1PutText
            // 
            this.R1PutText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1PutText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1PutText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1PutText.Location = new System.Drawing.Point(3, 27);
            this.R1PutText.MaxLength = 999999999;
            this.R1PutText.Multiline = true;
            this.R1PutText.Name = "R1PutText";
            this.R1PutText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.R1PutText.Size = new System.Drawing.Size(272, 382);
            this.R1PutText.TabIndex = 15;
            this.R1PutText.WordWrap = false;
            // 
            // R1PutLabel
            // 
            this.R1PutLabel.AutoSize = true;
            this.R1PutLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1PutLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1PutLabel.Location = new System.Drawing.Point(3, 5);
            this.R1PutLabel.Name = "R1PutLabel";
            this.R1PutLabel.Size = new System.Drawing.Size(45, 18);
            this.R1PutLabel.TabIndex = 14;
            this.R1PutLabel.Text = "PUT";
            // 
            // R1ResponsePanel
            // 
            this.R1ResponsePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ResponsePanel.Controls.Add(this.R1ResponseText);
            this.R1ResponsePanel.Controls.Add(this.R1ResponseLabel);
            this.R1ResponsePanel.Location = new System.Drawing.Point(972, 196);
            this.R1ResponsePanel.Name = "R1ResponsePanel";
            this.R1ResponsePanel.Size = new System.Drawing.Size(280, 418);
            this.R1ResponsePanel.TabIndex = 16;
            // 
            // R1ResponseText
            // 
            this.R1ResponseText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ResponseText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ResponseText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1ResponseText.Location = new System.Drawing.Point(3, 27);
            this.R1ResponseText.MaxLength = 999999999;
            this.R1ResponseText.Multiline = true;
            this.R1ResponseText.Name = "R1ResponseText";
            this.R1ResponseText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.R1ResponseText.Size = new System.Drawing.Size(272, 386);
            this.R1ResponseText.TabIndex = 21;
            this.R1ResponseText.TextChanged += new System.EventHandler(this.R1ResponseText_TextChanged);
            // 
            // R1ResponseLabel
            // 
            this.R1ResponseLabel.AutoSize = true;
            this.R1ResponseLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ResponseLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1ResponseLabel.Location = new System.Drawing.Point(3, 6);
            this.R1ResponseLabel.Name = "R1ResponseLabel";
            this.R1ResponseLabel.Size = new System.Drawing.Size(104, 18);
            this.R1ResponseLabel.TabIndex = 21;
            this.R1ResponseLabel.Text = "RESPONSE";
            // 
            // DeleteIdPanel
            // 
            this.DeleteIdPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DeleteIdPanel.Controls.Add(this.DeleteIdButton);
            this.DeleteIdPanel.Controls.Add(this.DeleteIdText);
            this.DeleteIdPanel.Controls.Add(this.DeleteIdTextLabel);
            this.DeleteIdPanel.Controls.Add(this.DeleteIdLabel);
            this.DeleteIdPanel.Location = new System.Drawing.Point(23, 332);
            this.DeleteIdPanel.Name = "DeleteIdPanel";
            this.DeleteIdPanel.Size = new System.Drawing.Size(342, 119);
            this.DeleteIdPanel.TabIndex = 13;
            // 
            // DeleteIdButton
            // 
            this.DeleteIdButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.DeleteIdButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DeleteIdButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.DeleteIdButton.FlatAppearance.BorderSize = 0;
            this.DeleteIdButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteIdButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteIdButton.ForeColor = System.Drawing.Color.White;
            this.DeleteIdButton.Location = new System.Drawing.Point(254, 83);
            this.DeleteIdButton.Name = "DeleteIdButton";
            this.DeleteIdButton.Size = new System.Drawing.Size(75, 24);
            this.DeleteIdButton.TabIndex = 11;
            this.DeleteIdButton.Text = "DELETE";
            this.DeleteIdButton.UseVisualStyleBackColor = false;
            this.DeleteIdButton.Click += new System.EventHandler(this.DeleteIdButton_Click);
            // 
            // DeleteIdText
            // 
            this.DeleteIdText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DeleteIdText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteIdText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.DeleteIdText.Location = new System.Drawing.Point(40, 45);
            this.DeleteIdText.Name = "DeleteIdText";
            this.DeleteIdText.Size = new System.Drawing.Size(289, 23);
            this.DeleteIdText.TabIndex = 12;
            // 
            // DeleteIdTextLabel
            // 
            this.DeleteIdTextLabel.AutoSize = true;
            this.DeleteIdTextLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteIdTextLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.DeleteIdTextLabel.Location = new System.Drawing.Point(12, 47);
            this.DeleteIdTextLabel.Name = "DeleteIdTextLabel";
            this.DeleteIdTextLabel.Size = new System.Drawing.Size(22, 16);
            this.DeleteIdTextLabel.TabIndex = 11;
            this.DeleteIdTextLabel.Text = "ID";
            // 
            // DeleteIdLabel
            // 
            this.DeleteIdLabel.AutoSize = true;
            this.DeleteIdLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteIdLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.DeleteIdLabel.Location = new System.Drawing.Point(12, 15);
            this.DeleteIdLabel.Name = "DeleteIdLabel";
            this.DeleteIdLabel.Size = new System.Drawing.Size(76, 18);
            this.DeleteIdLabel.TabIndex = 0;
            this.DeleteIdLabel.Text = "DELETE";
            // 
            // GetIdPanel
            // 
            this.GetIdPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GetIdPanel.Controls.Add(this.GetIdButton);
            this.GetIdPanel.Controls.Add(this.GetIdText);
            this.GetIdPanel.Controls.Add(this.GetIdTextLabel);
            this.GetIdPanel.Controls.Add(this.GetIdLabel);
            this.GetIdPanel.Location = new System.Drawing.Point(23, 196);
            this.GetIdPanel.Name = "GetIdPanel";
            this.GetIdPanel.Size = new System.Drawing.Size(342, 119);
            this.GetIdPanel.TabIndex = 10;
            // 
            // GetIdButton
            // 
            this.GetIdButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.GetIdButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.GetIdButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.GetIdButton.FlatAppearance.BorderSize = 0;
            this.GetIdButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GetIdButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetIdButton.ForeColor = System.Drawing.Color.White;
            this.GetIdButton.Location = new System.Drawing.Point(254, 83);
            this.GetIdButton.Name = "GetIdButton";
            this.GetIdButton.Size = new System.Drawing.Size(75, 24);
            this.GetIdButton.TabIndex = 11;
            this.GetIdButton.Text = "GET";
            this.GetIdButton.UseVisualStyleBackColor = false;
            this.GetIdButton.Click += new System.EventHandler(this.GetIdButton_Click);
            // 
            // GetIdText
            // 
            this.GetIdText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GetIdText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetIdText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.GetIdText.Location = new System.Drawing.Point(40, 45);
            this.GetIdText.Name = "GetIdText";
            this.GetIdText.Size = new System.Drawing.Size(289, 23);
            this.GetIdText.TabIndex = 12;
            // 
            // GetIdTextLabel
            // 
            this.GetIdTextLabel.AutoSize = true;
            this.GetIdTextLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetIdTextLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.GetIdTextLabel.Location = new System.Drawing.Point(12, 47);
            this.GetIdTextLabel.Name = "GetIdTextLabel";
            this.GetIdTextLabel.Size = new System.Drawing.Size(22, 16);
            this.GetIdTextLabel.TabIndex = 11;
            this.GetIdTextLabel.Text = "ID";
            // 
            // GetIdLabel
            // 
            this.GetIdLabel.AutoSize = true;
            this.GetIdLabel.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetIdLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.GetIdLabel.Location = new System.Drawing.Point(12, 15);
            this.GetIdLabel.Name = "GetIdLabel";
            this.GetIdLabel.Size = new System.Drawing.Size(44, 18);
            this.GetIdLabel.TabIndex = 0;
            this.GetIdLabel.Text = "GET";
            // 
            // R1RefreshButton
            // 
            this.R1RefreshButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1RefreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1RefreshButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1RefreshButton.FlatAppearance.BorderSize = 0;
            this.R1RefreshButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1RefreshButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1RefreshButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1RefreshButton.ForeColor = System.Drawing.Color.White;
            this.R1RefreshButton.Location = new System.Drawing.Point(573, 92);
            this.R1RefreshButton.Name = "R1RefreshButton";
            this.R1RefreshButton.Size = new System.Drawing.Size(75, 24);
            this.R1RefreshButton.TabIndex = 9;
            this.R1RefreshButton.Text = "Refresh";
            this.R1RefreshButton.UseVisualStyleBackColor = false;
            this.R1RefreshButton.Click += new System.EventHandler(this.R1RefreshButton_Click);
            // 
            // R1GoButton
            // 
            this.R1GoButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1GoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1GoButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1GoButton.FlatAppearance.BorderSize = 0;
            this.R1GoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1GoButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1GoButton.ForeColor = System.Drawing.Color.White;
            this.R1GoButton.Location = new System.Drawing.Point(492, 136);
            this.R1GoButton.Name = "R1GoButton";
            this.R1GoButton.Size = new System.Drawing.Size(75, 24);
            this.R1GoButton.TabIndex = 8;
            this.R1GoButton.Text = "Go";
            this.R1GoButton.UseVisualStyleBackColor = false;
            this.R1GoButton.Click += new System.EventHandler(this.R1GoButton_Click);
            // 
            // R1SaveButton
            // 
            this.R1SaveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.R1SaveButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1SaveButton.FlatAppearance.BorderSize = 0;
            this.R1SaveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1SaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1SaveButton.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1SaveButton.ForeColor = System.Drawing.Color.White;
            this.R1SaveButton.Location = new System.Drawing.Point(492, 92);
            this.R1SaveButton.Name = "R1SaveButton";
            this.R1SaveButton.Size = new System.Drawing.Size(75, 24);
            this.R1SaveButton.TabIndex = 7;
            this.R1SaveButton.Text = "Save";
            this.R1SaveButton.UseVisualStyleBackColor = false;
            this.R1SaveButton.Click += new System.EventHandler(this.R1SaveButton_Click);
            // 
            // FunctionCombo
            // 
            this.FunctionCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.FunctionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.FunctionCombo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FunctionCombo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FunctionCombo.FormattingEnabled = true;
            this.FunctionCombo.Items.AddRange(new object[] {
            "GET Company Info",
            "GET Tax Groups",
            "GET Classifications",
            "GET Templates/Invoice",
            "GET Templates/Estimates",
            "GET Templates/Bill",
            "GET Templates/BillCredit",
            "GET Templates/CreditNote",
            "GET Templates/ExpenseClaim",
            "GET Accounts",
            "GET Bank Accounts",
            "GET Contacts",
            "GET Items",
            "GET Journals",
            "GET Bank Transfers",
            "GET Payments",
            "GET Receipts",
            "GET Activity Statements",
            "GET Estimates",
            "GET Invoices",
            "GET Credit Notes",
            "GET Bills",
            "GET Supplier Credit Notes",
            "GET Terms",
            "GET Projects",
            "GET Timesheets",
            "GET Expense Claims",
            "",
            "POST Accounts",
            "POST Bank Accounts",
            "POST Contacts",
            "POST Items",
            "POST Journals",
            "POST Bank Transfers",
            "POST Payments(cash)",
            "POST Payments(allocation)",
            "POST Receipts(cash)",
            "POST Receipts(allocation)",
            "POST Estimates",
            "POST Invoices",
            "POST Credit Notes",
            "POST Bills",
            "POST Supplier Credit Notes",
            "POST Projects",
            "",
            "PUT Accounts",
            "PUT Contacts",
            "PUT Items",
            "PUT Journals",
            "PUT Payments",
            "PUT Receipts",
            "PUT Invoices",
            "PUT Estimates",
            "PUT Bills",
            "",
            "DELETE Accounts",
            "DELETE Contacts",
            "DELETE Items",
            "DELETE Journals",
            "DELETE Bank Transfers",
            "DELETE Payments",
            "DELETE Receipts",
            "DELETE Estimates",
            "DELETE Invoices",
            "DELETE Credit Notes",
            "DELETE Bills",
            "DELETE Supplier Credit Notes"});
            this.FunctionCombo.Location = new System.Drawing.Point(159, 136);
            this.FunctionCombo.Name = "FunctionCombo";
            this.FunctionCombo.Size = new System.Drawing.Size(323, 24);
            this.FunctionCombo.TabIndex = 6;
            this.FunctionCombo.SelectedIndexChanged += new System.EventHandler(this.FunctionCombo_SelectedIndexChanged);
            // 
            // BooksCombo
            // 
            this.BooksCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.BooksCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.BooksCombo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BooksCombo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.BooksCombo.FormattingEnabled = true;
            this.BooksCombo.Location = new System.Drawing.Point(159, 92);
            this.BooksCombo.Name = "BooksCombo";
            this.BooksCombo.Size = new System.Drawing.Size(323, 24);
            this.BooksCombo.TabIndex = 5;
            this.BooksCombo.SelectedIndexChanged += new System.EventHandler(this.BooksCombo_SelectedIndexChanged);
            // 
            // FunctionLabel
            // 
            this.FunctionLabel.AutoSize = true;
            this.FunctionLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FunctionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.FunctionLabel.Location = new System.Drawing.Point(20, 139);
            this.FunctionLabel.Name = "FunctionLabel";
            this.FunctionLabel.Size = new System.Drawing.Size(128, 16);
            this.FunctionLabel.TabIndex = 4;
            this.FunctionLabel.Text = "Choose a function";
            // 
            // BookLabel
            // 
            this.BookLabel.AutoSize = true;
            this.BookLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BookLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.BookLabel.Location = new System.Drawing.Point(83, 95);
            this.BookLabel.Name = "BookLabel";
            this.BookLabel.Size = new System.Drawing.Size(65, 16);
            this.BookLabel.TabIndex = 3;
            this.BookLabel.Text = "MyBooks";
            // 
            // R1AppLabel
            // 
            this.R1AppLabel.AutoSize = true;
            this.R1AppLabel.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1AppLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1AppLabel.Location = new System.Drawing.Point(12, 32);
            this.R1AppLabel.Name = "R1AppLabel";
            this.R1AppLabel.Size = new System.Drawing.Size(156, 29);
            this.R1AppLabel.TabIndex = 2;
            this.R1AppLabel.Text = "Reckon One";
            // 
            // R1ApHbPanel
            // 
            this.R1ApHbPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ApHbPanel.Controls.Add(this.R1ApHbStatus);
            this.R1ApHbPanel.Controls.Add(this.R1ApHbLabel);
            this.R1ApHbPanel.Location = new System.Drawing.Point(1129, 140);
            this.R1ApHbPanel.Name = "R1ApHbPanel";
            this.R1ApHbPanel.Size = new System.Drawing.Size(123, 27);
            this.R1ApHbPanel.TabIndex = 1;
            // 
            // R1ApHbStatus
            // 
            this.R1ApHbStatus.AutoSize = true;
            this.R1ApHbStatus.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApHbStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ApHbStatus.Location = new System.Drawing.Point(87, 6);
            this.R1ApHbStatus.Name = "R1ApHbStatus";
            this.R1ApHbStatus.Size = new System.Drawing.Size(0, 14);
            this.R1ApHbStatus.TabIndex = 5;
            this.R1ApHbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // R1ApHbLabel
            // 
            this.R1ApHbLabel.AutoSize = true;
            this.R1ApHbLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApHbLabel.Location = new System.Drawing.Point(16, 6);
            this.R1ApHbLabel.Name = "R1ApHbLabel";
            this.R1ApHbLabel.Size = new System.Drawing.Size(80, 14);
            this.R1ApHbLabel.TabIndex = 4;
            this.R1ApHbLabel.Text = "Heartbeat: ";
            this.R1ApHbLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // R1ApAuthPanel
            // 
            this.R1ApAuthPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ApAuthPanel.Controls.Add(this.R1ApAuthLabel);
            this.R1ApAuthPanel.Location = new System.Drawing.Point(1129, 114);
            this.R1ApAuthPanel.Name = "R1ApAuthPanel";
            this.R1ApAuthPanel.Size = new System.Drawing.Size(123, 27);
            this.R1ApAuthPanel.TabIndex = 1;
            // 
            // R1ApAuthLabel
            // 
            this.R1ApAuthLabel.AutoSize = true;
            this.R1ApAuthLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.R1ApAuthLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApAuthLabel.Location = new System.Drawing.Point(24, 6);
            this.R1ApAuthLabel.Name = "R1ApAuthLabel";
            this.R1ApAuthLabel.Size = new System.Drawing.Size(75, 14);
            this.R1ApAuthLabel.TabIndex = 3;
            this.R1ApAuthLabel.Text = "Auth Again";
            this.R1ApAuthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.R1ApAuthLabel.Click += new System.EventHandler(this.R1ApAuthLabel_Click);
            // 
            // R1ApRvkPanel
            // 
            this.R1ApRvkPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ApRvkPanel.Controls.Add(this.R1ApRvkLabel);
            this.R1ApRvkPanel.Location = new System.Drawing.Point(1129, 90);
            this.R1ApRvkPanel.Name = "R1ApRvkPanel";
            this.R1ApRvkPanel.Size = new System.Drawing.Size(123, 27);
            this.R1ApRvkPanel.TabIndex = 1;
            // 
            // R1ApRvkLabel
            // 
            this.R1ApRvkLabel.AutoSize = true;
            this.R1ApRvkLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.R1ApRvkLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApRvkLabel.Location = new System.Drawing.Point(13, 4);
            this.R1ApRvkLabel.Name = "R1ApRvkLabel";
            this.R1ApRvkLabel.Size = new System.Drawing.Size(99, 14);
            this.R1ApRvkLabel.TabIndex = 2;
            this.R1ApRvkLabel.Text = "Revoke Access";
            this.R1ApRvkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.R1ApRvkLabel.Click += new System.EventHandler(this.R1ApRvkLabel_Click);
            // 
            // R1ApRfshPanel
            // 
            this.R1ApRfshPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ApRfshPanel.Controls.Add(this.R1ApRfshLabel);
            this.R1ApRfshPanel.Location = new System.Drawing.Point(1129, 64);
            this.R1ApRfshPanel.Name = "R1ApRfshPanel";
            this.R1ApRfshPanel.Size = new System.Drawing.Size(123, 27);
            this.R1ApRfshPanel.TabIndex = 1;
            // 
            // R1ApRfshLabel
            // 
            this.R1ApRfshLabel.AutoSize = true;
            this.R1ApRfshLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.R1ApRfshLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApRfshLabel.Location = new System.Drawing.Point(14, 6);
            this.R1ApRfshLabel.Name = "R1ApRfshLabel";
            this.R1ApRfshLabel.Size = new System.Drawing.Size(96, 14);
            this.R1ApRfshLabel.TabIndex = 1;
            this.R1ApRfshLabel.Text = "Refresh Token";
            this.R1ApRfshLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.R1ApRfshLabel.Click += new System.EventHandler(this.R1ApRfshLabel_Click);
            // 
            // R1ApConnPanel
            // 
            this.R1ApConnPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ApConnPanel.Controls.Add(this.R1ApConnLabel);
            this.R1ApConnPanel.Location = new System.Drawing.Point(1129, 38);
            this.R1ApConnPanel.Name = "R1ApConnPanel";
            this.R1ApConnPanel.Size = new System.Drawing.Size(123, 27);
            this.R1ApConnPanel.TabIndex = 1;
            // 
            // R1ApConnLabel
            // 
            this.R1ApConnLabel.AutoSize = true;
            this.R1ApConnLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApConnLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.R1ApConnLabel.Location = new System.Drawing.Point(19, 6);
            this.R1ApConnLabel.Name = "R1ApConnLabel";
            this.R1ApConnLabel.Size = new System.Drawing.Size(0, 14);
            this.R1ApConnLabel.TabIndex = 0;
            this.R1ApConnLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // R1ApHeaderPanel
            // 
            this.R1ApHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(106)))));
            this.R1ApHeaderPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.R1ApHeaderPanel.Controls.Add(this.R1ApHeaderLabel);
            this.R1ApHeaderPanel.Location = new System.Drawing.Point(1129, 12);
            this.R1ApHeaderPanel.Name = "R1ApHeaderPanel";
            this.R1ApHeaderPanel.Size = new System.Drawing.Size(123, 27);
            this.R1ApHeaderPanel.TabIndex = 0;
            // 
            // R1ApHeaderLabel
            // 
            this.R1ApHeaderLabel.AutoSize = true;
            this.R1ApHeaderLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1ApHeaderLabel.ForeColor = System.Drawing.Color.White;
            this.R1ApHeaderLabel.Location = new System.Drawing.Point(18, 5);
            this.R1ApHeaderLabel.Name = "R1ApHeaderLabel";
            this.R1ApHeaderLabel.Size = new System.Drawing.Size(87, 16);
            this.R1ApHeaderLabel.TabIndex = 0;
            this.R1ApHeaderLabel.Text = "Admin Panel";
            this.R1ApHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.R1AppPanel);
            this.Controls.Add(this.RahAppPanel);
            this.Controls.Add(this.RahConnPanel);
            this.Controls.Add(this.R1ConnPanel);
            this.Controls.Add(this.HomePanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Reckon API Sample App";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.HomePanel.ResumeLayout(false);
            this.HomePanel.PerformLayout();
            this.RahConnPanel.ResumeLayout(false);
            this.RahConnPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RahConnHome)).EndInit();
            this.R1ConnPanel.ResumeLayout(false);
            this.R1ConnPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1ConnHome)).EndInit();
            this.RahAppPanel.ResumeLayout(false);
            this.RahAppPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RahAppHome)).EndInit();
            this.RahRqIdPanel.ResumeLayout(false);
            this.RahRqIdPanel.PerformLayout();
            this.RahRespPanel.ResumeLayout(false);
            this.RahRespPanel.PerformLayout();
            this.RahBodyPanel.ResumeLayout(false);
            this.RahBodyPanel.PerformLayout();
            this.RahFileDtlsPanel.ResumeLayout(false);
            this.RahFileDtlsPanel.PerformLayout();
            this.RahApHbPanel.ResumeLayout(false);
            this.RahApHbPanel.PerformLayout();
            this.RahApAuthPanel.ResumeLayout(false);
            this.RahApAuthPanel.PerformLayout();
            this.RahApRvkPanel.ResumeLayout(false);
            this.RahApRvkPanel.PerformLayout();
            this.RahApRfshPanel.ResumeLayout(false);
            this.RahApRfshPanel.PerformLayout();
            this.RahApConnPanel.ResumeLayout(false);
            this.RahApConnPanel.PerformLayout();
            this.RahApHeaderPanel.ResumeLayout(false);
            this.RahApHeaderPanel.PerformLayout();
            this.R1AppPanel.ResumeLayout(false);
            this.R1AppPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1AppHome)).EndInit();
            this.R1PostPanel.ResumeLayout(false);
            this.R1PostPanel.PerformLayout();
            this.R1PutPanel.ResumeLayout(false);
            this.R1PutPanel.PerformLayout();
            this.R1ResponsePanel.ResumeLayout(false);
            this.R1ResponsePanel.PerformLayout();
            this.DeleteIdPanel.ResumeLayout(false);
            this.DeleteIdPanel.PerformLayout();
            this.GetIdPanel.ResumeLayout(false);
            this.GetIdPanel.PerformLayout();
            this.R1ApHbPanel.ResumeLayout(false);
            this.R1ApHbPanel.PerformLayout();
            this.R1ApAuthPanel.ResumeLayout(false);
            this.R1ApAuthPanel.PerformLayout();
            this.R1ApRvkPanel.ResumeLayout(false);
            this.R1ApRvkPanel.PerformLayout();
            this.R1ApRfshPanel.ResumeLayout(false);
            this.R1ApRfshPanel.PerformLayout();
            this.R1ApConnPanel.ResumeLayout(false);
            this.R1ApConnPanel.PerformLayout();
            this.R1ApHeaderPanel.ResumeLayout(false);
            this.R1ApHeaderPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel HomePanel;
        private System.Windows.Forms.Button R1AppSelectButton;
        private System.Windows.Forms.Button RahAppSelectButton;
        private System.Windows.Forms.Label WelcomeLabel;
        private System.Windows.Forms.Panel RahConnPanel;
        private System.Windows.Forms.Button RahConnButton;
        private System.Windows.Forms.Label RahConnLabel;
        private System.Windows.Forms.Panel R1ConnPanel;
        private System.Windows.Forms.Button R1ConnButton;
        private System.Windows.Forms.Label R1ConnLabel;
        private System.Windows.Forms.Panel RahAppPanel;
        private System.Windows.Forms.Panel RahApHbPanel;
        private System.Windows.Forms.Label RahApHbLabel;
        private System.Windows.Forms.Panel RahApAuthPanel;
        private System.Windows.Forms.Label RahApAuthLabel;
        private System.Windows.Forms.Panel RahApRvkPanel;
        private System.Windows.Forms.Label RahApRvkLabel;
        private System.Windows.Forms.Panel RahApRfshPanel;
        private System.Windows.Forms.Label RahApRfshLabel;
        private System.Windows.Forms.Panel RahApConnPanel;
        private System.Windows.Forms.Label RahApConnLabel;
        private System.Windows.Forms.Panel RahApHeaderPanel;
        private System.Windows.Forms.Label RahApHeaderLabel;
        private System.Windows.Forms.Label RahApHbStatus;
        private System.Windows.Forms.Panel R1AppPanel;
        private System.Windows.Forms.Label R1AppLabel;
        private System.Windows.Forms.Panel R1ApHbPanel;
        private System.Windows.Forms.Label R1ApHbStatus;
        private System.Windows.Forms.Label R1ApHbLabel;
        private System.Windows.Forms.Panel R1ApAuthPanel;
        private System.Windows.Forms.Label R1ApAuthLabel;
        private System.Windows.Forms.Panel R1ApRvkPanel;
        private System.Windows.Forms.Label R1ApRvkLabel;
        private System.Windows.Forms.Panel R1ApRfshPanel;
        private System.Windows.Forms.Label R1ApRfshLabel;
        private System.Windows.Forms.Panel R1ApConnPanel;
        private System.Windows.Forms.Label R1ApConnLabel;
        private System.Windows.Forms.Panel R1ApHeaderPanel;
        private System.Windows.Forms.Label R1ApHeaderLabel;
        private System.Windows.Forms.Label RahAppLabel;
        private System.Windows.Forms.Button R1RefreshButton;
        private System.Windows.Forms.Button R1SaveButton;
        private System.Windows.Forms.ComboBox FunctionCombo;
        private System.Windows.Forms.Label FunctionLabel;
        private System.Windows.Forms.Label BookLabel;
        private System.Windows.Forms.Panel R1PostPanel;
        private System.Windows.Forms.TextBox R1PostText;
        private System.Windows.Forms.Label R1PostLabel;
        private System.Windows.Forms.Panel R1PutPanel;
        private System.Windows.Forms.TextBox R1PutText;
        private System.Windows.Forms.Label R1PutLabel;
        private System.Windows.Forms.Panel R1ResponsePanel;
        private System.Windows.Forms.TextBox R1ResponseText;
        private System.Windows.Forms.Label R1ResponseLabel;
        private System.Windows.Forms.Panel DeleteIdPanel;
        private System.Windows.Forms.TextBox DeleteIdText;
        private System.Windows.Forms.Label DeleteIdTextLabel;
        private System.Windows.Forms.Label DeleteIdLabel;
        private System.Windows.Forms.Panel GetIdPanel;
        private System.Windows.Forms.TextBox GetIdText;
        private System.Windows.Forms.Label GetIdTextLabel;
        private System.Windows.Forms.Label GetIdLabel;
        private System.Windows.Forms.Panel RahRqIdPanel;
        private System.Windows.Forms.Label RahRqIdLabel;
        private System.Windows.Forms.Label RahRqIdLabelSmall;
        private System.Windows.Forms.Panel RahRespPanel;
        private System.Windows.Forms.Label RahRespLabel;
        private System.Windows.Forms.Panel RahBodyPanel;
        private System.Windows.Forms.Label RahBodyLabel;
        private System.Windows.Forms.Panel RahFileDtlsPanel;
        private System.Windows.Forms.Label FilePwdLabel;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.Label FileUserLabel;
        private System.Windows.Forms.Label FilePathLabel;
        private System.Windows.Forms.PictureBox RahConnHome;
        private System.Windows.Forms.PictureBox R1ConnHome;
        private System.Windows.Forms.PictureBox RahAppHome;
        private System.Windows.Forms.PictureBox R1AppHome;
        private System.Windows.Forms.ComboBox BooksCombo;
        private System.Windows.Forms.Label RahFunctionsLabel;
        public System.Windows.Forms.TextBox RahBodyText;
        public System.Windows.Forms.ComboBox VersionCombo;
        public System.Windows.Forms.Button FileSaveButton;
        private System.Windows.Forms.Button DeleteIdButton;
        private System.Windows.Forms.Button R1GoButton;
        private System.Windows.Forms.Button R1ClearButton;
        private System.Windows.Forms.Button R1PutButton;
        private System.Windows.Forms.Button R1PostButton;
        private System.Windows.Forms.Button GetIdButton;
        public System.Windows.Forms.Button RahClearButton;
        public System.Windows.Forms.TextBox RahRqIdText;
        public System.Windows.Forms.TextBox RahRespText;
        public System.Windows.Forms.TextBox FilePwdText;
        public System.Windows.Forms.TextBox FileUserText;
        public System.Windows.Forms.TextBox FilePathText;
        public System.Windows.Forms.ComboBox RahFunctionsCombo;
        public System.Windows.Forms.Button RahPostButton;
        public System.Windows.Forms.Button RahGetIdButton;
    }
}

