﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Reckon_API_Sample_App
{
    //Class holding reckon one functions
    class ReckonOne
    {
        //Create Form1 instance to access Form1
        private Form1 _instance;
        public ReckonOne(Form1 instance)
        {
            _instance = instance;
        }

        public static string BookId { get; set; }
        public static string BookName { get; set; }
        public static string BookStatus { get; set; }
        public static string Country { get; set; }
        public static string R1SubscriptionKey { get; } = ConfigurationManager.AppSettings["r1SubscriptionKey"].ToString();
        public static string R1ApiSegment { get; } = "/R1";
        public static string R1Heartbeat { get; } = "/heartbeat";
        public static string CashBooksSegment { get; } = "/cashbooks";
        public static string CompanyInfo { get; } = "/companyinfo";
        public static string TaxGroups { get; } = "/taxgroups";
        public static string Classifications { get; } = "/classifications";
        public static string TemplatesInvoice { get; } = "/templates/invoice";
        public static string TemplateEstimates { get; } = "/templates/estimate";
        public static string TemplatesBill { get; } = "/templates/bill";
        public static string TemplatesBillCredit { get; } = "/templates/billcredit";
        public static string TemplatesCreditNote { get; } = "/templates/creditnote";
        public static string TemplatesExpenseClaim { get; } = "/templates/expenseclaim";
        public static string Accounts { get; } = "/accounts";
        public static string BankAccounts { get; } = "/bankaccounts";
        public static string Contacts { get; } = "/contacts";
        public static string Items { get; } = "/items";
        public static string Journals { get; } = "/journals";
        public static string BankTransfers { get; } = "/banktransfers";
        public static string Payments { get; } = "/payments";
        public static string Receipts { get; } = "/receipts";
        public static string ActivityStatements { get; } = "/activitystatements";
        public static string Estimates { get; } = "/estimates";
        public static string Invoices { get; } = "/invoices";
        public static string CreditNotes { get; } = "/creditnotes";
        public static string Bills { get; } = "/bills";
        public static string SupplierCreditNotes { get; } = "/suppliercreditnotes";
        public static string Terms { get; } = "/terms";
        public static string Projects { get; } = "/projects";
        public static string TimeSheets { get; } = "/timesheets/summary";
        public static string ExpenseClaims { get; } = "/expenseclaims";

        public static string Server { get; } = ConfigurationManager.AppSettings["server"].ToString();
        public static string Database { get; } = ConfigurationManager.AppSettings["database"].ToString();
        public static string User { get; } = ConfigurationManager.AppSettings["user"].ToString();
        public static string Password { get; } = ConfigurationManager.AppSettings["password"].ToString();
        public static string Port { get; } = ConfigurationManager.AppSettings["port"].ToString();
        public static string SSLMode { get; } = ConfigurationManager.AppSettings["sslMode"].ToString();

        //Check R1 Heartbeat
        public void R1Hearbeat()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var response = httpClient.GetAsync($"{ Globals.ApiBaseUrl }{ R1ApiSegment }{ R1Heartbeat }").Result;
                Globals.R1Status = response.StatusCode.ToString();
            }
        }

        //Get CashBooks List for combobox
        public void GetBooks()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, R1SubscriptionKey);
                var response = httpClient.GetAsync($"{ Globals.ApiBaseUrl }{ R1ApiSegment }{ CashBooksSegment }").Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                JArray parsedArray = JArray.Parse(responseText);
                foreach (JObject parsedObject in parsedArray.Children<JObject>())
                {
                    foreach (var parsedProperty in parsedObject.Properties())
                    {
                        if (parsedProperty.Name == "BookId") BookId = parsedProperty.Value.ToString();
                        if (parsedProperty.Name == "BookName") BookName = parsedProperty.Value.ToString();
                        if (parsedProperty.Name == "Bookstatus") BookStatus = parsedProperty.Value.ToString();
                        if (parsedProperty.Name == "Country") Country = parsedProperty.Value.ToString();
                    }
                    using (MySqlConnection mySqlConnection = new MySqlConnection())
                    {
                        mySqlConnection.ConnectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", Server, Port, User, Password, Database, SSLMode);
                        mySqlConnection.Open();
                        string sqlQuery = "INSERT INTO cashbooks VALUES (@BookId, @BookName, @Bookstatus, @Country) " +
                            "ON DUPLICATE KEY UPDATE BookId=@BookId, BookName=@BookName, Bookstatus=@Bookstatus, Country=@Country";
                        MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, mySqlConnection);
                        mySqlCommand.Parameters.AddWithValue("@BookId", BookId);
                        mySqlCommand.Parameters.AddWithValue("@BookName", BookName);
                        mySqlCommand.Parameters.AddWithValue("@Bookstatus", BookStatus);
                        mySqlCommand.Parameters.AddWithValue("@Country", Country);
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                        mySqlConnection.Close();
                    }
                }
            }
            using (MySqlConnection mySqlConnection = new MySqlConnection())
            {
                mySqlConnection.ConnectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", Server, Port, User, Password, Database, SSLMode);
                mySqlConnection.Open();
                string sqlQuery = "SELECT BookId, BookName FROM cashbooks";
                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(sqlQuery, mySqlConnection);
                DataSet dataSet = new DataSet();
                mySqlDataAdapter.Fill(dataSet);
                _instance.BooksComboboxAddItem(dataSet);
            }
        }

        //Function Selection Sets Which R1 Endpoint To Use
        public void FunctionSelect(string function)
        {
            string uRl = $"{Globals.ApiBaseUrl}{R1ApiSegment}";
            string objectId = "/" + _instance.R1DeleteId;

            switch (function)
            {
                case "GET Company Info":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, CompanyInfo);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Tax Groups":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, $"{TaxGroups}/{DateTime.Now.ToString("yyyy-MM-dd")}");
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Classifications":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Classifications);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Templates/Invoice":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TemplatesInvoice);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Templates/Estimates":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TemplateEstimates);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Templates/Bill":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TemplatesBill);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Templates/BillCredit":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TemplatesBillCredit);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Templates/CreditNote":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TemplatesCreditNote);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Templates/ExpenseClaim":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TemplatesExpenseClaim);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Accounts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Accounts);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Bank Accounts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, BankAccounts);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Contacts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Contacts);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Items":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Items);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Journals":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Journals);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Bank Transfers":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, BankTransfers);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Payments":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Payments);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Receipts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Receipts);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Activity Statements":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, ActivityStatements);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Estimates":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Estimates);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Invoices":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Invoices);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Credit Notes":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, CreditNotes);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Bills":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Bills);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Supplier Credit Notes":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, SupplierCreditNotes);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Terms":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Terms);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Projects":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Projects);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Timesheets":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TimeSheets);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "GET Expense Claims":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, ExpenseClaims);
                    Globals.SelectedFunctionType = "GET";
                    break;
                case "POST Accounts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Accounts);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"AccountName\":\"\",\r\n\"AccountType\":,\r\n\"Status\":1\r\n}";
                    break;
                case "POST Bank Accounts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, BankAccounts);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"AccountType\":,\r\n\"BankAccountStatus\":1,\r\n\"BulkElectronicPaymentSettings\":\r\n\t{\r\n\t\"AccountName\":\"\"\r\n\t}, \r\n\"Description\":\"\", \r\n\"EFT\": \r\n\t{\r\n\t}, \r\n\"OpeningBalance\": 0.0\r\n}";
                    break;
                case "POST Contacts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Contacts);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"Description\":\"\",\r\n\"FirstNameBranchName\":\"\",\r\n\"SurnameBusinessName\":\"\",\r\n\"IsCustomer\":true,\r\n\"IsSupplier\":true,\r\n\"IsActive\":true,\r\n\"IsPerson\":false\r\n}";
                    break;
                case "POST Items":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Items);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"Name\":\"\",\r\n\"ItemStatus\":true,\r\n\"ItemType\":1,\r\n\"IsTaxInclusive\":false,\r\n\"IsPurchased\":true,\r\n\"IsSold\":true,\r\n\"SoldOrPurchased\":3,\r\n\"PurchasePrice\":0.00,\r\n\"PurchaseCategoryId\":\"Cost of Goods Sold\",\r\n\"PurchaseTaxId\":\"NCG\",\r\n\"PurchaseDescription\":\"\",\r\n\"SalePrice\":0.00,\r\n\"SaleCategoryId\":\"Sales\",\r\n\"SaleTaxId\":\"GST\",\r\n\"SaleDescription\":\"\"\r\n}";
                    break;
                case "POST Journals":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Journals);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"AmountTaxStatus\": 2,\r\n\"JournalDate\":\"0000-00-00T00:00:00+00:00\",\r\n\"JournalStatus\":2,\r\n\"Summary\":\"\",\r\n\"Transactions\":[\r\n\t{\r\n\t\"AccountingCategoryId\":\"\",\r\n\t\"Credit\":0.00\r\n\t},\r\n\t{\r\n\t\"AccountingCategoryId\":\"\",\r\n\t\"Debit\":0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Bank Transfers":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, BankTransfers);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"TransferDate\":\"0000-00-00\",\r\n\"Description\":\"\",\r\n\"FromBankAccountId\":\"\",\r\n\"FromReconciliationStatus\":0,\r\n\"ToBankAccountId\":\"\",\r\n\"ToReconciliationStatus\":0,\r\n\"Amount\":0.00\r\n}";
                    break;
                case "POST Payments(cash)":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Payments);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"TransDate\": \"0000-00-00T00:00:00+00:00\", \r\n\"BankAccountId\": \"\", \r\n\"Amount\": 0.00, \r\n\"ContactId\": \"\", \r\n\"AllocateFullAmount\": true,\r\n\"AllocationSplit\":[\r\n\t{\r\n\t\"AccountId\": \"\",\r\n\t\"LineNo\": 1, \r\n\t\"Quantity\": 1,\r\n\t\"TaxGroupId\": \"\", \r\n\t\"AmountExTax\": 0.00,\r\n\t\"TaxAmount\": 0.00, \r\n\t\"HasDataForPosting\": true\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Payments(allocation)":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Payments);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"TransDate\": \"0000-00-00\",\r\n\"BankAccountId\":\"\",\r\n\"Amount\":0.00,\r\n\"ContactId\":\"\",\r\n\"AllocateFullAmount\": true,\r\n\"Details\": \"\",\r\n\"Reference\": \"\", \r\n\"IsReconciled\": false, \r\n\"Narration\": \"\", \r\n\"Allocations\": [\r\n\t{\r\n\t\"AllocationAmount\": 0.00,\r\n\t\"AllocationType\": \"Bill\",\r\n\t\"AllocationTypeId\": 2, \r\n\t\"RefId\": \"\"\r\n\t}\r\n],\r\n\"AllocationSplit\": [],\r\n\"AccountsPayableCategoryId\": \"Accounts Payable\"\r\n}";
                    break;
                case "POST Receipts(cash)":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Receipts);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"TransDate\":\"0000-00-00\",\r\n\"BankAccountId\":\"\",\r\n\"Amount\":0.00,\r\n\"ContactId\":\"\",\r\n\"AllocateFullAmount\":true,\r\n\"AllocationSplit\":[\r\n\t{\r\n\t\"AccountId\":\"\",\r\n\t\"LineNo\":1,\r\n\t\"Quantity\":1,\r\n\t\"TaxGroupId\":\"\",\r\n\t\"AmountExTax\":0.00,\r\n\t\"TaxAmount\":0.00,\r\n\t\"HasDataForPosting\":true\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Receipts(allocation)":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Receipts);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"TransDate\":\"0000-00-00\",\r\n\"BankAccountId\": \"\",\r\n\"Amount\": 0.00,\r\n\"ContactId\": \"\",\r\n\"AllocateFullAmount\": true,\r\n\"Allocations\":[\r\n\t{\r\n\t\"AllocationAmount\": 0.00,\r\n\t\"AllocationType\": \"Invoice\",\r\n\t\"AllocationTypeId\": 1,\r\n\t\"RefId\": \"\"\r\n\t}\r\n],\r\n\"AllocationSplit\": [],\r\n\"AccountsReceivableCategoryId\": \"Accounts Receivable\"\r\n}";
                    break;
                case "POST Estimates":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Estimates);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"CustomerId\": \"\",\r\n\"EstimateDate\": \"0000-00-00T00:00:00\",\r\n\"EstimateAmount\": 0.00,\r\n\"AmountTaxStatus\": 2,\r\n\"EstimateTax\": 0.00,\r\n\"TemplateId\": \"\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"Amount\": 0.00,\r\n\t\"ChargeableItemId\": \"\",\r\n\t\"LineNo\": 1,\r\n\t\"Description\": \"\",\r\n\t\"Quantity\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.00,\r\n\t\"UnitPriceIsTaxInclusive\": false,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"TaxIsModified\": false,\r\n\t\"TaxAmount\": 0.00,\r\n\t\"AmountExTax\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Invoices":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Invoices);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"CustomerID\": \"\",\r\n\"InvoiceAmount\": 0.00,\r\n\"AmountTaxStatus\": 2,\r\n\"InvoiceDate\": \"0000-00-00\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"Amount\": 0.00,\r\n\t\"AccountId\": \"\",\r\n\t\"TaxGroupId\": \"\",\r\n\t\"AutoCalcTax\": true\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Credit Notes":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, CreditNotes);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"CustomerId\": \"\",\r\n\"AmountTaxStatus\": 2,\r\n\"Amount\": 0.00,\r\n\"Tax\": 0.00,\r\n\"TemplateId\": \"\",\r\n\"CreditNoteDate\": \"0000-00-00T00:00:00\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"Amount\": 0.00,\r\n\t\"InvoiceDiscountedAmountExTax\": 0.00,\r\n\t\"InvoiceDiscountedTaxAmount\": 0.00,\r\n\t\"HasDataForPosting\": true,\r\n\t\"AccountId\": \"\", \r\n\t\"LineNo\": 1,\r\n\t\"Description\": null,\r\n\t\"Quantity\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.00,\r\n\t\"UnitPriceIsTaxInclusive\": false,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"TaxIsModified\": false,\r\n\t\"TaxAmount\": 0.00,\r\n\t\"AmountExTax\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Bills":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Bills);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"SupplierId\":\"\",\r\n\"AmountTaxStatus\":2,\r\n\"AccountsPayableAccountingCategoryId\":\"\",\r\n\"Status\":2,\r\n\"BillAmount\":0.00,\r\n\"BillTax\":0.00,\r\n\"TemplateId\":\"\",\r\n\"BillDate\":\"0000-00-00T00:00:00\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"AccountingCategoryID\":\"\",\r\n\t\"LineNo\":1,\r\n\t\"Description\":\"\",\r\n\t\"Quantity\":1,\r\n\t\"UnitPriceExTax\":0.00,\r\n\t\"UnitPriceTax\":0.00,\r\n\t\"TaxGroupId\":\"\",\r\n\t\"TaxIsModified\":false,\r\n\t\"AmountExTax\":0.00,\r\n\t\"Amount\":0.00,\r\n\t\"TaxAmount\":0.00,\r\n\t\"InvoiceDiscountedAmountExTax\":0.00,\r\n\t\"InvoiceDiscountedTaxAmount\":0.00,\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Supplier Credit Notes":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, SupplierCreditNotes);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"SupplierId\": \"\",\r\n\"AmountTaxStatus\": 2,\r\n\"Amount\": 0.00,\r\n\"Tax\": 0.00,\r\n\"TemplateId\": \"\",\r\n\"SupplierCreditNoteDate\": \"0000-00-00T00:00:00\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"AccountingCategoryId\": \"\",\r\n\t\"Amount\": 0.00,\r\n\t\"InvoiceDiscountedAmountExTax\": 0.00,\r\n\t\"InvoiceDiscountedTaxAmount\": 0.00,\r\n\t\"LineNo\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.00,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"TaxAmount\": 0.00,\r\n\t\"AmountExTax\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "POST Projects":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Projects);
                    Globals.SelectedFunctionType = "POST";
                    _instance.R1Body = "{\r\n\"Name\": \"\",\r\n\"StartDate\": \"0000-00-00\",\r\n\"EndDate\": \"0000-00-00\",\r\n\"Status\": 0,\r\n\"Description\": \"\",\r\n\"ProjectItems\": [],\r\n\"Contacts\": []\r\n}";
                    break;
                //case "POST Timesheets":
                //    Globals.SelectedFunction = string.Format("{0}{1}{2}{3}", uRl, Globals.SelectedBook, TimeSheets, "/week");
                //    Globals.SelectedFunctionType = "POST";
                //    _instance.R1Body = "{\r\n\"EmployeeId\": \"\",\r\n\"StartDate\": \"0000-00-00\",\r\n\"Timesheets\":[\r\n\t{\r\n\t\"CustomerId\": \"\",\r\n\t\"ItemId\": \"\",\r\n\t\"Billable\": true,\r\n\t\"Day1\": \"00:00\",\r\n\t\"Day2\": \"00:00\",\r\n\t\"Day3\": \"00:00\",\r\n\t\"Day4\": \"00:00\",\r\n\t\"Day5\": \"00:00\",\r\n\t\"Day6\": null,\r\n\t\"Day7\": null\r\n\t}\r\n]\r\n}";
                //    break;
                //case "POST Expense Claims":
                //    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, ExpenseClaims);
                //    Globals.SelectedFunctionType = "POST";
                //    _instance.R1Body = "{\r\n\"EmployeeContactId\": \"\",\r\n\"CustomerId\": \"\",\r\n\"Status\": 1,\r\n\"ClaimDate\": \"0000-00-00T00:00:00\",\r\n\"TemplateId\": \"\",\r\n\"AccountsPayableAccountingCategoryId\": \"\",\r\n\"ExpenseClaimAmount\": 0.00,\r\n\"ExpenseClaimTax\": 0.00,\r\n\"AmountTaxStatus\": 2,\r\n\"ExpenseClaimItems\":[\r\n\t{\r\n\t\"Date\": \"0000-00-00T00:00:00\",\r\n\t\"SupplierID\": \"\",\r\n\t\"CustomerID\": \"\",\r\n\t\"IsBillable\": false,\r\n\t\"Status\": 1,\r\n\t\"Amount\": 0.00, \r\n\t\"InvoiceDiscountedAmountExTax\": 0.00,\r\n\t\"InvoiceDiscountedTaxAmount\": 0.00,\r\n\t\"AccountId\": \"\",\r\n\t\"LineNo\": 1,\r\n\t\"Description\": \"\",\r\n\t\"TaxGroupId\": \"\",\r\n\t\"TaxAmount\": 0.00,\r\n\t\"AmountExTax\": 10.0\r\n\t}\r\n]\r\n}";
                //    break;
                case "PUT Accounts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Accounts);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"Id\":\"\",\r\n\"AccountType\":,\r\n\"AccountName\" :\"\",\r\n\"Hint\" :\"\",\r\n\"Status\":1,\r\n\"TaxGroupId\" :\"\",\r\n\"AccountCode\":\"\"\r\n}";
                    break;
                case "PUT Contacts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Contacts);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"ContactId\":\"\",\r\n\"Description\":\"\",\r\n\"SurnameBusinessName\":\"\",\r\n\"IsCustomer\":true,\r\n\"IsSupplier\":true,\r\n\"IsActive\":true\r\n}";
                    break;
                case "PUT Items":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Items);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"Id\": \"\",\r\n\"Name\":\"\",\r\n\"ItemStatus\":1,\r\n\"ItemType\":,\r\n\"SoldOrPurchased\":,\r\n\"SaleCategoryId\": \"\"\r\n}";
                    break;
                case "PUT Journals":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Journals);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"JournalId\":\"\",\r\n\"AmountTaxStatus\": 2,\r\n\"JournalStatus\": 2,\r\n\"JournalNumber\":\"\",\r\n\"Summary\":\"\",\r\n\"JournalDate\":\"0000-00-00\",\r\n\"Transactions\":[\r\n\t{\r\n\t\"AccountingCategoryId\":\"\",\r\n\t\"Debit\":0.00\r\n\t},\r\n\t{\r\n\t\"AccountingCategoryId\":\"\",\r\n\t\"Credit\":0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "PUT Payments":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Payments);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"Id\":\"\",\r\n\"TransDate\": \"0000-00-00\",\r\n\"BankAccountId\": \"\",\r\n\"Amount\": 0.00,\r\n\"ContactId\": \"\",\r\n\"AllocateFullAmount\": false,\r\n\"AccountsPayableCategoryId\": \"\",\r\n\"Allocations\":[],\r\n\"AllocationSplit\":[\r\n\t{\r\n\t\"ChargeableItemId\": \"\",\r\n\t\"LineNo\": 1,\r\n\t\"Quantity\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.00,\r\n\t\"UnitPricePrecision\": 2,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"AmountExTax\": 0.00,\r\n\t\"Amount\": 0.00,\r\n\t\"TaxAmount\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "PUT Receipts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Receipts);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"Id\":\"\",\r\n\"TransDate\": \"0000-00-00\",\r\n\"BankAccountId\": \"\",\r\n\"Amount\": 0.00,\r\n\"ContactId\": \"\",\r\n\"AllocateFullAmount\": false,\r\n\"AccountsReceivableCategoryId\": \"\",\r\n\"Allocations\":[],\r\n\"AllocationSplit\":[\r\n\t{\r\n\t\"ChargeableItemId\": \"\",\r\n\t\"LineNo\": 1,\r\n\t\"Quantity\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.00,\r\n\t\"UnitPricePrecision\": 2,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"AmountExTax\": 0.00,\r\n\t\"Amount\": 0.00,\r\n\t\"TaxAmount\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "PUT Estimates":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Estimates);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"EstimateId\":\"\",\r\n\"CustomerId\": \"\",\r\n\"EstimateDate\": \"000-00-00T00:00:00\",\r\n\"EstimateAmount\": 0.00,\r\n\"AmountTaxStatus\": 2,\r\n\"EstimateTax\": 0.00,\r\n\"TemplateId\": \"\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"Amount\": 0.00,\r\n\t\"AccountId\": null,\r\n\t\"ChargeableItemId\": \"\",\r\n\t\"LineNo\": 1,\r\n\t\"Description\": \"\",\r\n\t\"Quantity\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.0,\r\n\t\"UnitPricePrecision\": 2,\r\n\t\"UnitPriceIsTaxInclusive\": false,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"TaxAmount\": 0.00,\r\n\t\"AmountExTax\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "PUT Invoices":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Invoices);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"Id\":\"\",\r\n\"CustomerId\": \"\",\r\n\"AmountTaxStatus\":2,\r\n\"Status\":2,\r\n\"InvoiceAmount\":0.00,\r\n\"InvoiceTax\":0.00,\r\n\"InvoiceDate\":\"0000-00-00T00:00:00\",\r\n\"DueDate\":\"0000-00-00T00:00:00\",\r\n\"TemplateId\": \"\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"Amount\": 0.00,\r\n\t\"ChargeableItemId\": \"\",\r\n\t\"LineNo\": 1,\r\n\t\"Quantity\": 1,\r\n\t\"UnitPriceExTax\": 0.00,\r\n\t\"UnitPriceTax\": 0.0,\r\n\t\"TaxGroupId\": \"\",\r\n\t\"TaxAmount\": 0.00,\r\n\t\"AmountExTax\": 0.00\r\n\t}\r\n]\r\n}";
                    break;
                case "PUT Bills":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Bills);
                    Globals.SelectedFunctionType = "PUT";
                    _instance.R1Put = "{\r\n\"BillId\": \"\",\r\n\"BillNumber\": \"\",\r\n\"SupplierId\":\"\",\r\n\"AmountTaxStatus\":2,\r\n\"AccountsPayableAccountingCategoryId\":\"\",\r\n\"Status\":2,\r\n\"BillAmount\":0.00,\r\n\"BillTax\":0.00,\r\n\"TemplateId\":\"\",\r\n\"BillDate\":\"0000-00-00T00:00:00\",\r\n\"LineItems\":[\r\n\t{\r\n\t\"AccountingCategoryID\":\"\",\r\n\t\"LineNo\":1,\r\n\t\"Description\":\"0\",\r\n\t\"Quantity\":1,\r\n\t\"UnitPriceExTax\":0.00,\r\n\t\"UnitPriceTax\":0.00,\r\n\t\"TaxGroupId\":\"\",\r\n\t\"TaxIsModified\":false,\r\n\t\"AmountExTax\":0.00,\r\n\t\"Amount\":0.00,\r\n\t\"TaxAmount\":0.00,\r\n\t\"InvoiceDiscountedAmountExTax\":0.00,\r\n\t\"InvoiceDiscountedTaxAmount\":0.00\r\n\t}\r\n]\r\n}";
                    break;
                //case "PUT Timesheets":
                //    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TimeSheets);
                //    Globals.SelectedFunctionType = "PUT";
                //    _instance.R1Put = "{\r\n\"EmployeeId\": \"\",\r\n\"Date\": \"0000-00-00\",\r\n\"Entries\":[\r\n\t{\r\n\t\"TimesheetEntryId\": \"\",\r\n\t\"Date\": \"0000-00-00\",\r\n\t\"CustomerId\": \"\",\r\n\t\"ItemId\": \"\",\r\n\t\"Notes\": \"\",\r\n\t\"Time\": \"00:00\",\r\n\t\"Billable\": true,\r\n\t\"Status\": 2\r\n\t}\r\n]\r\n}";
                //    break;
                case "DELETE Accounts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Accounts);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Contacts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Contacts);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Items":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Items);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Journals":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Journals);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Bank Transfers":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, BankTransfers);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Payments":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Payments);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Receipts":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Receipts);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Estimates":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Estimates);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Invoices":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Invoices);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Credit Notes":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, CreditNotes);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Bills":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, Bills);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                case "DELETE Supplier Credit Notes":
                    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, SupplierCreditNotes);
                    Globals.SelectedFunctionType = "DELETE";
                    break;
                //case "DELETE Timesheets":
                //    Globals.SelectedFunction = string.Format("{0}{1}{2}", uRl, Globals.SelectedBook, TimeSheets);
                //    Globals.SelectedFunctionType = "DELETE";
                //    break;
            }
        }

        //R1 Get Request Function
        public void R1GetAPIRequest(string objectId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, R1SubscriptionKey);
                var response = httpClient.GetAsync(string.Format("{0}{1}", Globals.SelectedFunction, objectId)).Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                string responseStatusCode = response.StatusCode.ToString();
                if (responseStatusCode == "OK")
                {
                    _instance.R1GoButtonEnabled = true;
                    _instance.R1GetIdButtonEnabled = true;
                    _instance.R1GoButtonText = "GO";
                    _instance.R1GoButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1GetIdButtonText = "GET";
                    _instance.R1GetIdButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1Resp = responseText;
                }
                else
                {
                    _instance.R1GoButtonEnabled = true;
                    _instance.R1GetIdButtonEnabled = true;
                    _instance.R1GoButtonText = "GO";
                    _instance.R1GoButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1GetIdButtonText = "GET";
                    _instance.R1GetIdButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("There was an error getting data from " + Globals.SelectedFunction + 
                        ". The response status code is " + responseStatusCode, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        //R1 Get by ID Request Function
        public void R1GetByIdRequest()
        {
            if (_instance.FunctionsCombo == "GET Accounts")
            {
                Globals.SelectedFunction = string.Format("{0}?$filter=Id eq {1}", Globals.SelectedFunction, _instance.R1GetId);
                R1GetAPIRequest("");
            }
            else if (_instance.FunctionsCombo == "GET Projects")
            {
                Globals.SelectedFunction = string.Format("{0}?$filter=ID eq {1}", Globals.SelectedFunction, _instance.R1GetId);
                R1GetAPIRequest("");
            }
            else if (_instance.FunctionsCombo == "GET Items")
            {
                Globals.SelectedFunction = string.Format("{0}?$filter=Id eq {1}", Globals.SelectedFunction, _instance.R1GetId);
                R1GetAPIRequest("");
            }
            else if (_instance.FunctionsCombo == "GET Bank Accounts")
            {
                Globals.SelectedFunction = string.Format("{0}?$filter=BankAccountID eq {1}", Globals.SelectedFunction, _instance.R1GetId);
                R1GetAPIRequest("");
            }
            else R1GetAPIRequest($"/ { _instance.R1GetId }");
        }

        //R1 Post Request Function
        public void R1PostAPIRequest()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                StringContent postBody = new StringContent(_instance.R1Body, Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, R1SubscriptionKey);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = httpClient.PostAsync(Globals.SelectedFunction, postBody).Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                string responseStatusCode = response.StatusCode.ToString();
                if (responseStatusCode == "OK")
                {

                    _instance.R1PostButtonEnabled = true;
                    _instance.R1PostButtonText = "POST";
                    _instance.R1PostButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1Resp = responseStatusCode + "\r\n\r\n" + responseText;
                }
                else
                {
                    _instance.R1PostButtonEnabled = true;
                    _instance.R1PostButtonText = "POST";
                    _instance.R1PostButtonBackColor = Color.FromArgb(0, 189, 157);
                    //TESTING WITH SEEING RESP TEXT
                    _instance.R1Resp = responseStatusCode + "\r\n\r\n" + responseText;
                    MessageBox.Show("There was an error posting data to " + Globals.SelectedFunction + 
                        ". The response status code is " + responseStatusCode, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        //R1 Put Request Function
        public void R1PutAPIRequest()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Form1 form1 = new Form1();
                StringContent putBody = new StringContent(_instance.R1Put, Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, R1SubscriptionKey);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = httpClient.PutAsync(Globals.SelectedFunction, putBody).Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                string responseStatusCode = response.StatusCode.ToString();
                string reasonPhrase = response.ReasonPhrase.ToString();
                if (responseStatusCode == "OK" )
                {
                    _instance.R1PutButtonEnabled = true;
                    _instance.R1PutButtonText = "PUT";
                    _instance.R1PutButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1Resp = "Your Request was Successfull.\r\n\r\nResponse Code: " + responseStatusCode;
                }
                else if(reasonPhrase == "OK")
                {
                    _instance.R1PutButtonEnabled = true;
                    _instance.R1PutButtonText = "PUT";
                    _instance.R1PutButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1Resp = "Your Request was Successfull.\r\n\r\nResponse Code: " + reasonPhrase;
                }
                else
                {
                    _instance.R1PutButtonEnabled = true;
                    _instance.R1PutButtonText = "PUT";
                    _instance.R1PutButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("There was an error posting data to " + Globals.SelectedFunction + 
                        ". The response status code is " + responseStatusCode, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        //R1 Delete Request Function
        public void R1DeleteAPIRequest()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Form1 form1 = new Form1();
                string objectId = "/" + _instance.R1DeleteId;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorize.AccessToken);
                httpClient.DefaultRequestHeaders.Add(Globals.SubscriptionKeyType, R1SubscriptionKey);
                var response = httpClient.DeleteAsync($"{ Globals.SelectedFunction }{ objectId }").Result;
                string responseText = response.Content.ReadAsStringAsync().Result;
                string responseStatusCode = response.StatusCode.ToString();
                string reasonPhrase = response.ReasonPhrase.ToString();
                if (responseStatusCode == "OK")
                {
                    _instance.R1DeleteIdButtonEnabled = true;
                    _instance.R1DeleteIdButtonText = "DELETE";
                    _instance.R1DeleteIdButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1Resp = "Your Request was Successfull.\r\n\r\nResponse Code: " + responseStatusCode;
                }
                else if (reasonPhrase == "OK")
                {
                    _instance.R1PutButtonEnabled = true;
                    _instance.R1PutButtonText = "PUT";
                    _instance.R1PutButtonBackColor = Color.FromArgb(0, 189, 157);
                    _instance.R1Resp = "Your Request was Successfull.\r\n\r\nResponse Code: " + reasonPhrase;
                }
                else
                {
                    _instance.R1DeleteIdButtonEnabled = true;
                    _instance.R1DeleteIdButtonText = "DELETE";
                    _instance.R1DeleteIdButtonBackColor = Color.FromArgb(0, 189, 157);
                    MessageBox.Show("There was an error deleting data from " + Globals.SelectedFunction + 
                        ". The response status code is " + responseStatusCode, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
